import json
from datetime import datetime, timedelta, timezone

# Opening JSON file
f = open('discord_history.json')

# returns JSON object as
# a dictionary
data = json.load(f)

# Iterating through the json
# list of messages from newest to oldest
messages = []
for message in data:
    content = message['content'].encode('utf-8').decode('utf-8')
    timestamp = None
    try:
        timestamp = datetime.strptime(
            message['timestamp'], '%Y-%m-%dT%H:%M:%S.%f%z')
    except:
        timestamp = datetime.strptime(
            message['timestamp'], '%Y-%m-%dT%H:%M:%f%z')
    date = timestamp.astimezone(
        timezone(timedelta(hours=2))).strftime('%d/%m/%Y %H:%M')
    author = message['author']['username']
    link = "https://discord.com/channels/@me/" + \
        str(message['channel_id']) + "/" + str(message['id'])

    if content and len(content) > 0 and content[0] == '(' and content[-1] == ')':
        continue
    if content.startswith('https://'):
        continue

    to_write = '### %s\n\n##### %s\n\n##### <%s>\n\n%s\n' % (
        author, date, link, content)
    messages.append(to_write)

# Closing file
f.close()

f = open('messages.md', 'w', encoding='utf-8')
for compt in range(len(messages)-1, -1, -1):
    f.write(messages[compt])
