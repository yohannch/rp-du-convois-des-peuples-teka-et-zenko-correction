### MamaBallou

##### 21/04/2023 14:17

##### <https://discord.com/channels/@me/503896082045206528/1098942967701590066>

**Les kitsunes avait comme beaucoup d'autres peuples une partie de sa population nomade. Cet exode perpétuel des kitsunes avait commencé bien longtemps avant la naissance de la jeune femme. Les kitsunes appelaient cette époque Rīku (la fuite). À cette époque, le peuple des orcs avait débarqué sur leurs terres, saccageant tout sur leur passage. La fuite avait duré presque 10 ans avant qu'une coalition se forme entre les peuples devenus nomades et que les orcs soient chassés sur des terres isolées et enfermés à l'aide d'une barrière magique.**

**Suite à ce conflit, au vue de la puissance magique de certaines créatures et par peur de représailles. Les représentant les plus puissants et jugés dangereux avaient été contraint à la vie de nomades. Bien sûr, certains s'étaient indignés d'une telle décision des leurs. Mais ceux qui y adhéraient avaient "accompagné" leur prise de décision dans la direction souhaitée.**//ça ne veut rien dire.

**De nos jours, seuls les anciens se souviennent de ce temps et les plus jeunes n'en sont que la mémoire. Pourtant leur mode de vie est resté et ainsi, ils parcourent le monde.**

**Depuis plusieurs lunes, les kitsunes ont rejoint les dryades pour un bout de chemin jusqu'au royaume voisin où ils devaient tous passer l'hiver.**
//un BOUT DE CHEMIN, mec on est en Mai on est plus proche du précédent Hiver que du prochain,on parle d'une migration de plus de 6 mois. Ils ont aussi mis 6 moi à revenirde  l'ancienne ?

### Mathius

##### 22/04/2023 01:06

##### <https://discord.com/channels/@me/503896082045206528/1099108996041936986>

**Les deux peuples ont alors dû apprendre à cohabiter durant ces diverses étapes de leur migration commune. Cela est d'autant plus compliqué en comparant les deux cultures, en effet, les deux peuplades,**//c'est lourd **ont chacunes leurs propres mythologies et leur propre idéologies. En effet, les dryades semblent respecter un mysticisme axé autour de diverses idoles, une forme de culte que nos lecteurs pourront aisément comparer à du polythéisme tandis que la peuplade des kitsunes semblait respecter une autre forme de mysticisme. Cela rendit l'émulsion relativement complexe, surtout quand on se penche sur les jeunes membres des deux tribux devant alors se confronter à une vision si différente de la leur. Cette différence est par moment particulièrement handicapante puisque les deux peuplades ont leurs cycles propres et leurs propres désidératas au niveau du trajet. Ici après une rapide présentation des croyances des Kitsunes nous allons nous concentrer justement sur les jeunes membres de ces tribus qui sont confrontés à cette différence culturelle.**

### MamaBallou

##### 22/04/2023 01:29

##### <https://discord.com/channels/@me/503896082045206528/1099114720235098235>

Évidemment que je vous raconte la vérité ! *s'étonna-t-elle* Cette femme offre son corps et elle a l'honneur de bénéficier de la protection de la Déesse Inari. Elle fera le voyage avec nous et mangera à notre table jusqu'à ce qu'elle arrive au prochain village en toute sécurité.

**Hisako qui venait de s'exprimer défendait une jeune prostituée habillée de rouge, hébergée dans sa roulotte que certains jeunes dryades avaient moquées. En effet, les kitsunes ont pour habitude, pendant leurs pérégrinations, de prendre sous leur aile des prostituées de toutes races pour les aider à changer leur lieu de résidence au besoin. Leur déesse promettant protection à celles-ci, les kitsunes prenaient toujours soin de leur offrir leurs services et un toit. Parfois, la prostituée offrait ses services pendant le voyage, non sans déplaire aux kitsunes.**

### Mathius

##### 22/04/2023 12:21

##### <https://discord.com/channels/@me/503896082045206528/1099278715394998343>

**Adaram faisait alors partie du 'public' qui avait assisté à cette prise de parole de cette Kitsune. Il n'appréciait pas forcément le peuple des Kitsune ni leurs moeurs, mais il ne faisait pas parti des 'accusés'.**//Acusateur? **Cependant, cela le choquait tant car au sein des dryades plusieurs divinités étaient reconnues et pour la plupart, elle avait un ensemble de 'commandements' qui étaient récités par un 'apôtre' (Nommé ici par 'représentant') de la divinité en question. L'une des divinités la plus respectée n'était qu'autre que la divinité associée au foyer, Efya, prônant au sein de ses commandements de ne vendre son corps en aucun cas. Cela révulsait donc la dryade que nous suivons.**

*Adaram s'avança légèrement et dit sur un ton qui se veut relativement apaisé* Nous ne doutons pas de vos croyances, cependant le 'métier' de cette jeune femme est par définition proscrit par l'une de nos divinité Efya, une des divinités les plus importante aux yeux de grand nombre de dryades, nous ne lui feront pas de mal mais ne t'attends pas à ce que nous la traitions avec déférence.

*(Ici les guillemets autour de mots prononcé par Adaram signifie qu'il emphase le mot en question donc soit pour l'accentuer soit comme ici pour souligner qu'il a une forme de dédain dans la voix en prononçant ce mot)*

### MamaBallou

##### 22/04/2023 12:45

##### <https://discord.com/channels/@me/503896082045206528/1099284869206519878>

*Hisako plissa les yeux de défiance* Et tous vous agglutiner ainsi autour de la roulote c'est ça votre 'métier'. Vous n'avez rien de mieux à faire. *ses oreilles étaient plaquées en arrière et sa queue s'était hérissée* Cette femme n'est pas une bête curieuse mais une invité donc un peu de respect. Et si vous n'en êtes pas capable faite comme si elle n'était pas là. *elle se redresse et reprend un posture plus décontractée* Ce ne peut pas être difficile, regardez. *elle tourne la tête de côté* Maintenant, vous n'existez plus.

**Sa tête boudeuse serais très mignonne, si nous aurions fait abstraction de son caractère précédemment observé**

### Mathius

##### 22/04/2023 12:54

##### <https://discord.com/channels/@me/503896082045206528/1099287076358340628>

*Une partie des dryades commençaient à la toiser d'un regard mauvais tandis qu'Adaram levait les yeux au ciel. Durant la seconde partie du discours d'Hisako, une partie des dryades sont alors parties rageusement en mogréant des 'Quel culot !' et des 'Ces Kitsunes, quel peuple décadent' tandis qu'Adaram avec un ton de défiance s'adresse encore une fois à Hisako* Nous ne vous souhaitons pas du mal, mais vos moeurs étant *cherche ses mots un instant* différentes, nous ne traiterons pas vos invités de la même façon en suivant nos propres moeurs *avant de retourner vers la roulotte d'où il venait, situé à une quizaine de mettre de celle d'Hisako*

### MamaBallou

##### 22/04/2023 13:06

##### <https://discord.com/channels/@me/503896082045206528/1099290144084873280>

*Hisako soupira de soulagement quand ils s'en furent aller et grommela pour elle même* Je ne viens pas embêter vos invités, je ne vois pas quel est le problème...
**Elle se secoua pour se débarrasser du stress qu'elle avait accumulé. S'étira et décida de rendre visite à leur invitée pour la rassurer. Une fois chose faite, la route repris. Il restait encore 4 jours avant d'arriver au village suivant. Le premier soir arriva et le cortège s'arrêta sur une plaine, sur le bord d'un cours d'eau. On pouvait voir l'orée d'une forêt à 100 mètres. Hisako en profita pour aller se dégourdir les jambes et ramasser du bois pour la cuisine. Elle déambulait dans la forêt, perdue dans ses pensées**

### Mathius

##### 22/04/2023 13:15

##### <https://discord.com/channels/@me/503896082045206528/1099292284283007047>

**Quand à lui, Adaram, à la suite de cet échange assez tendu, décida de vaquer à ses occupations, il aida sa communauté à préparer le repas du soir et diverses offrandes pour Efya, selon les anciens pour apaiser celle-ci. Adaram même si choqué par la profession de l'invitée des Kitsunes, ne comprenait pas pleinement sa réaction et celle de son peuple, il décida donc d'aller se recueillir au sein de la forêt pour chercher le concours de Geemko, divinité associée à une forme de méditation et d'introspection. N'importe qui serait alors décontenancé par le recueillement d'une dryade cherchant le coucours de Geemko puisque cela pouvait ressembler à une forme d'invocation vodoo puisqu'ici Adaram était en tailleur devant un grand arbre à marmonner en Kapt'ch (Le Language cérémoniel des Dryades) devant une forme d'offrande consistant en une forme d'encens.**

### MamaBallou

##### 22/04/2023 13:29

##### <https://discord.com/channels/@me/503896082045206528/1099296013086375978>

**Hisako capta un bruit qui la fit sortir de ses songes. Cela ressemblait à un vrombissement, un bruit lisse et répétitif. Elle s'approcha prudemment et, se cachant derrière un arbre, aperçu Adaram assit en tailleur. En y regardant mieux, il avait l'air concentré. Ne voulant pas le croiser depuis leur dernier échange. Elle commença à faire demi-tour, essayant d'ignorer le bruit de sa litanie. Mais quelque chose la dérangeait. Des craquements. Et ce n'était pas ses pas et certainement pas Adaram qui était assis. Pensant à un animal sauvage, elle regretta de ne pas avoir pris d'arc ou de lance pour avoir de la viande ce soir. Elle monta dans un arbre pour avoir une meilleure visibilité. C'est alors qu'elle le vit, un ours immense et noir. Il devait faire au moins 3 fois la taille d'un ours ordinaire. Tous ses sens lui crièrent alors de s'éloigner au plus vite de cet animal. Elle commença à descendre puis se souvint d'Adaram. Où allait l'ours, elle suivit son regard et se figeât. Droit vers lui. Tant pis, elle descendit et partit.**

**Après quelques mètres, elle fit demi tour** Et merde... *souffla-t-elle*//CLICHé

**Elle alla aussi vite que possible vers là où se trouvait Adaram, limitant au maximum le bruit de ses pas. Mais ce n'était pas assez. Elle pris donc une décision. Elle était maintenant plus petite, à quatre pattes et avec un museau. Elle s'extirpa de ses vêtements devenus trop grand et fila. Elle savait pertinemment qu'elle n'avait pas le droit mais c'était un cas de force majeur. Elle trouva Adaram dans la même posture, l'ours n'était pas encore arrivé mais elle l'entendait tout proche. Elle essaya d'attirer son attention en lui donnant des petits coup de tête dans le dos**

### Mathius

##### 22/04/2023 13:41

##### <https://discord.com/channels/@me/503896082045206528/1099299041206079498>

**Adaram était alors perdu dans cette forme d'introspection, en faisant quasi abstraction du reste de ses sens. Cependant alors que la concentration qu'il devait déployer pour la chose faiblissait un instant, certains de ses sens revinrent très brièvement. Il sentit alors des coups répétés dans son dos. 'Depuis combien de temps' fut la première pensée qui lui traversa l'esprit, il se tourna tout doucement vers l'animal avec une curiosité maladive sur le visage**

Oui ? *dit-il sur un ton détendu avant d'entendre les craquements puissants au loin* Tu as peur ? *en semblant parler des craquements*

### MamaBallou

##### 22/04/2023 13:44

##### <https://discord.com/channels/@me/503896082045206528/1099299784185094144>

La renarde hoche la tête et attrape sa manche entre ses dents et commence à le tirer avec précipitation en regardant affolée autour d'elle.

### Mathius

##### 22/04/2023 13:48

##### <https://discord.com/channels/@me/503896082045206528/1099300700971868242>

**Sans plus de bruit il récupère rapidement ce qui semble être de l'encens avant de se lever et de se mettre accroupi en face de l'animal.**

Je dois te suivre par là ? *murmura-t-il en pointant la direction d'où venait l'animal*

### MamaBallou

##### 22/04/2023 13:57

##### <https://discord.com/channels/@me/503896082045206528/1099303041347367052>

**La renard hoche la tête à nouveau et commence à avancer rapidement devant lui pour lui montrer le chemin. Espérant de tout cœur que l'ours ne les suive pas. Entendant ses pas se rapprocher encore.**

### Mathius

##### 22/04/2023 14:03

##### <https://discord.com/channels/@me/503896082045206528/1099304530161717331>

**Il la suit au rythme qu'elle exige en semblant ne pas faire de bruit alors que son visage semble de plus en plus inquiet en se demandant ce qui arrive à ce renard**

### MamaBallou

##### 22/04/2023 14:21

##### <https://discord.com/channels/@me/503896082045206528/1099309019715354634>

**Elle continue d'avancer puis senti son odeur. L'ours est tout près, ils n'ont pas le temps d'aller jusqu'aux roulottes. Alors, elle pique vers la droite et trouve un renfoncement derrière une butte qui pourrait faire office d'abris temporaire. Elle fait signe à Adaram de la suivre. Et s'arrête dans le renfoncement**

### Mathius

##### 22/04/2023 15:07

##### <https://discord.com/channels/@me/503896082045206528/1099320654026788875>

**Adaram la suit en se carapattant comme il peux alors qu'il fait malheureusement du bruit en se faufillant**

### MamaBallou

##### 22/04/2023 15:12

##### <https://discord.com/channels/@me/503896082045206528/1099321756298268672>

**La renarde le pousse d'avantage dans le renfoncement et retient son souffle. Elle sent les vibrations des pas lourds de l'ours qui passe à proximité. Ils peuvent le voir de là où ils sont. L'ours hume l'air et s'éloigne. Son cœur bat à toute allure**//Heueusement qu'il n'est pas attiré par l'encens.

### Mathius

##### 22/04/2023 17:42

##### <https://discord.com/channels/@me/503896082045206528/1099359590216376352>

**Elle entend le cœur d'Adaram battre à tout allure en voyant cet animal. Adaram est alors complètement paniqué alors qu'il se pousse et se presse contre elle comme s'il souhaitait rassurer cet animal. N'ayant donc pas vu l'ours mais l'entendant malgré tout**

### MamaBallou

##### 22/04/2023 17:46

##### <https://discord.com/channels/@me/503896082045206528/1099360652629377055>

**Quand tout fut enfin calme, la renarde se détendit et s'écarta de Adaram. Elle vérifia de nouveau que rien de dangereux n'était à les attendre puis elle pointa du museau la direction des roulottes, puis s'enfuit à toute allure, à la recherche de ses vêtements**

### Mathius

##### 22/04/2023 19:39

##### <https://discord.com/channels/@me/503896082045206528/1099388894576918639>

**Il l'observa interdit avant de prendre quelques minutes pour reprendre ses esprits et d'enfin s'épouster avant de se diriger vers sa roulotte en étant toujours perturbé par la situation**

### MamaBallou

##### 22/04/2023 20:17

##### <https://discord.com/channels/@me/503896082045206528/1099398604961099917>

**De son côté, elle réussit à retrouver ses vêtements et à revenir aux roulottes mais sans le bois qu'elle était partie chercher. Voyant cela, on la réprimanda. Heureusement après avoir expliqué ce qu'il s'était passé. Les représentants des kitsunes en parlèrent à ceux des dryades et une barrière fut érigée par mesure de précaution.**

**De plus, ce soir était la première pleine lune du mois de mai et le début de la saison des récoltes. Hisako comme les autres kitsunes s'était habillée de rouge, couleur de la déesse. Cette couleur souvent mal vue pour son écho à la couleur du sang allait particulièrement bien avec les cheveux ébènes des kitsunes.**

**La soirée commença ainsi avec des offrandes de riz puis s'en suivit des danses et des chants joyeux et entrainants. Évidemment, les dryades avaient été invités mais aucun n'avait encore osé se joindre à la célébration autrement que par des regards de jugement depuis leurs roulottes. D'autres encore lançaient des regards agacés à cause du bruit.**

### Mathius

##### 22/04/2023 23:22

##### <https://discord.com/channels/@me/503896082045206528/1099445036996907098>

**Adaram quant à lui avec les autres dryades a alors évoqué la situation si particulière qu'il avait vécu avec cet animal. Discours alors coupé par la venue des Kitsunes demandant de mettre en place une barrière. Cela surprit alors quelques dryades qui furent le rapprochement avec la situation dépeinte par Adaram. Ainsi une partie des dryades toisaient les kitsunes dû à la différence culturelle et une autre pour tenter de comprendre cette 'coiencidence'**//Quoi ? Ils les regardent de haut parce qu'ils peuvent se transformer ?

### MamaBallou

##### 22/04/2023 23:29

##### <https://discord.com/channels/@me/503896082045206528/1099446996219199639>

**Hisako qui dansait depuis un moment se décala sur le côté pour reprendre son souffle, elle aperçu et fixa Adaram de loin et ne pu s'empêcher d'éprouver un certain soulagement à le voir en pleine santé. Après presque une minute, elle sortit de ses pensées, se rendant compte qu'elle le fixait sûrement depuis trop longtemps et retourna danser et festoyer avec les autres**

### Mathius

##### 23/04/2023 01:24

##### <https://discord.com/channels/@me/503896082045206528/1099475776593723422>

**Adaram ne semble pas avoir perçu ce regard mais semble surprit par la cérémonie, il s'approche alors de la cérémonie en sortant de la roulotte, semble poser ce qui semble être une petite mousse avant de remonter dans la roulotte et de suivre du regard cette mousse.**

### MamaBallou

##### 23/04/2023 01:30

##### <https://discord.com/channels/@me/503896082045206528/1099477369607168000>

**Hisako continue de danser, virevoltant telle une toupie. Elle est comme prise d'une fièvre et danse si vite que ses pieds frôlent à peine de sol.**

### Mathius

##### 23/04/2023 02:08

##### <https://discord.com/channels/@me/503896082045206528/1099486984482852908>

**La mousse commença à se déplacer tout doucement vers le rituel des kitsunes**

### MamaBallou

##### 23/04/2023 02:10

##### <https://discord.com/channels/@me/503896082045206528/1099487356437925983>

**Elle continue de danser, ce qui la rapproche de ladite mousse sans qu'elle s'en aperçoive**

### Mathius

##### 23/04/2023 02:37

##### <https://discord.com/channels/@me/503896082045206528/1099494274384539748>

**Cette mousse se retrouve alors sur la scène où se déroule le rituel alors que certaines des dryades cessèrent lentement mais sûrement de toiser les kitsunes. Cependant Adaram à contrario sort à nouveau de la roulotte et s'approche légèrement, en restant donc à une distance respectable du rituel, pour observer et non toiser les kitsunes**

### MamaBallou

##### 23/04/2023 02:40

##### <https://discord.com/channels/@me/503896082045206528/1099494880214011954>

**Hisako danse toujours, l'air se distord légèrement autours d'elle. C'est alors que son pied rencontre la mousse**

### MamaBallou

##### 23/04/2023 02:47

##### <https://discord.com/channels/@me/503896082045206528/1099496617763152017>

**La mousse, douce sous son pied, ne la dérangea pas et elle continua sa danse. L'air était comme compressé autour d'elle. Cela se remarquait surtout quand elle passait devant une source de lumière. Certains kitsunes le remarquèrent et admiraient, contemplatifs du phénomène.**

### Mathius

##### 23/04/2023 02:51

##### <https://discord.com/channels/@me/503896082045206528/1099497783330877510>

**Adaram s'installa devant sa roulotte observant alors le phénomène jusqu'à la fin de cette phase dansante du rituel.**

### MamaBallou

##### 23/04/2023 02:53

##### <https://discord.com/channels/@me/503896082045206528/1099498300484358234>

**La danse se termina enfin, Hisako était haletante mais rayonnante. La soirée se termina sur une phase de recueillement et de chant calme. À la fin, elle resta un moment pour observer le ciel étoilé**

### Mathius

##### 23/04/2023 12:56

##### <https://discord.com/channels/@me/503896082045206528/1099649044135038976>

**Adaram supposa alors que la célébration des Kitsunes était achevée et s'approcha tout doucement de la place centrale. En direction de la fameuse mousse.**

### MamaBallou

##### 23/04/2023 12:56

##### <https://discord.com/channels/@me/503896082045206528/1099649978982473789>

**Sentant un mouvement non loin. Elle tourne la tête et voit Adaram s'approcher de la piste de danse improvisée utilisée plus tôt dans la soirée.**

### Mathius

##### 23/04/2023 13:13

##### <https://discord.com/channels/@me/503896082045206528/1099654296510935110>

**Elle peut alors le voir s'installer sur la piste et sembler poser ses mains au sol et marmonner quelque chose. En effet il semble interagir avec la fameuse mousse.**

### MamaBallou

##### 23/04/2023 13:22

##### <https://discord.com/channels/@me/503896082045206528/1099656647145685042>

**Se rapproche un peu pour mieux voir. Tout en gardant ses distances.**

### Mathius

##### 23/04/2023 13:27

##### <https://discord.com/channels/@me/503896082045206528/1099657910428127282>

**Elle peut alors s'approcher sans qu'Adaram ne s'en rende compte. Elle voit alors ce dernier les mains à même le sol sur la mousse en marmonnant du Kapt'ch et de progressivement déplacer ses mains sur la mousse**

### MamaBallou

##### 23/04/2023 13:30

##### <https://discord.com/channels/@me/503896082045206528/1099658588076654622>

**Décidant de ne pas l'interrompre, elle se contente d'observer et d'essayer de comprendre ce qu'il essaye de faire.**

### Mathius

##### 23/04/2023 13:43

##### <https://discord.com/channels/@me/503896082045206528/1099661759364731030>

**Elle continue de l'observer jusqu'à la fin du rituel d'Adaram qui dure quelques grosses minutes. Ce dernier semble reprendre partiellement ses esprits alors que le rituel a fait disparaître la mousse. Adaram reste alors interdit quelques instants en voyant Hisako l'observer.**

### MamaBallou

##### 23/04/2023 13:50

##### <https://discord.com/channels/@me/503896082045206528/1099663612643131422>

**Elle s'avance encore pour arriver à son niveau.**

Qu'est-ce que tu fabrique ? *demande-t-elle intriguée*

### Mathius

##### 23/04/2023 13:55

##### <https://discord.com/channels/@me/503896082045206528/1099664806778245120>

Je me .. recueillais à vrai dire. *dit-il d'un ton peu assuré en se demandant ce que souhaitait cette Kitsune*

### MamaBallou

##### 23/04/2023 13:59

##### <https://discord.com/channels/@me/503896082045206528/1099665758449061988>

Comment ça ? *insiste-t-elle, ne comprenant visiblement pas sa façon de faire.*

### Mathius

##### 23/04/2023 14:10

##### <https://discord.com/channels/@me/503896082045206528/1099668556536361023>

Suite à l'annonce de l'ours, une annonce donc d'un danger, on se recueille souvent auprès d'Itsaru, et c'est tout simplement l'une des façons de se recueillir *dit-il un peu hésitant malgré tout*
//Donc les dryades se recueillent en faisant apparaitre de la mousse, la regarder gambader pour finalement la refaire disparaitre ?

### MamaBallou

##### 23/04/2023 15:11

##### <https://discord.com/channels/@me/503896082045206528/1099684066388869170>

**Elle ne semble pas douter de ses propos et se contente de continuer la conversation**

Itsaru ? Tu peux m'expliquer ?

**Elle avait l'air innocemment curieuse.**

### Mathius

##### 23/04/2023 15:57

##### <https://discord.com/channels/@me/503896082045206528/1099695478284746864>

Itsaru est l'une des divinités que nous respectons, en l'occurrence, c'est la divinité protectrice des forêts, il est coutume alors de se recueillir à proximité d'un élément de la forêt comme de la mousse qui était ici//c'est toi qui l'a posé ici coco *Répond-t-il sans broncher et bien satisfait que cette dernière ne remette pas en cause le rituel*

### MamaBallou

##### 23/04/2023 16:01

##### <https://discord.com/channels/@me/503896082045206528/1099696496942780437>

Hm, d'accord. Oh pardon, je ne me suis pas présentée. Je m'appelle Hisako. *dit-elle en le saluant de la tête*

### Mathius

##### 23/04/2023 16:26

##### <https://discord.com/channels/@me/503896082045206528/1099702852605190194>

Moi non plus à vrai dire, je m'appelle Adaram. *en saluant à son tour*

### MamaBallou

##### 23/04/2023 16:29

##### <https://discord.com/channels/@me/503896082045206528/1099703585396232252>

Ravie de faire ta connaissance. Il faut que je te laisse, il commence à se faire tard. À plus tard peut-être ! *le salut-elle en commençant à s'éloigner*

### Mathius

##### 23/04/2023 16:32

##### <https://discord.com/channels/@me/503896082045206528/1099704384042053643>

Ravi également. *en la saluant surprit par cette interaction avant de à son tour commencer à se relever*

### MamaBallou

##### 23/04/2023 16:37

##### <https://discord.com/channels/@me/503896082045206528/1099705492730810429>

**Les 2 jours suivant se passèrent sans encombre. Quand arriva le 3e jour, ils voyaient enfin le village dans lequel il devait faire escale.**

### Mathius

##### 23/04/2023 16:51

##### <https://discord.com/channels/@me/503896082045206528/1099709183886098552>

**Adaram durant ces deux jours avait comme auparavant continué à déambuler au sein de la caravane, sans plus interagir que nécessaire avec le peuple Kitsune. Cependant, le reste des Dryades ne semblait pas aussi patient avec les Kitsunes, l'ambiance était délétère. Cela sembla se calmer au 3ème jour justement quand la prochaine escale se profilait à l'horizon. En effet la prochaine escale n'était autre qu'un village relativement modeste, probablement pas plus d'une centaine d'âmes, mais qui semblait bien fortifié**//100 ames c'est quoi, 10-15 familles donc pas plus d'une quinzaine de maison, c'est pas un village
c'est un simple hameau mais qui est carrément fortifié?

### MamaBallou

##### 23/04/2023 17:03

##### <https://discord.com/channels/@me/503896082045206528/1099712192628142170>

**Les kitsunes aidèrent la prostituée à s'installer dans ses nouveaux quartiers. Puis la laissèrent prendre ses aises tranquillement.**

**De son côté, Hisako déambulait dans le village pour voir ce que les marchants et artisans avaient à proposer.**

### Mathius

##### 23/04/2023 17:10

##### <https://discord.com/channels/@me/503896082045206528/1099713924032643072>

**Adaram resta bien sagement au sein du convoi, avant d'entrer à son tour dans le village mais quelques heures après les Kitsunes, il déambula à son tour au sein du marché, où l'on croisait majoritairement des Kobolds***(Lézard humanoïde si tu veut une caricature)***qui vendaient divers produits comme des poteries, quelques bijoux et surtout pas mal de nourritures et de plantes aussi bien médicinales qu'aromatiques**

### MamaBallou

##### 23/04/2023 17:24

##### <https://discord.com/channels/@me/503896082045206528/1099717344869228555>

**Hisako approcha d'un étal avec des bijoux et en repéra quelques-uns aux propriétés potentiellements intéressantes mais bien au dessus de ses moyens. Elle continua donc et se posa sur le côté du tumulte pour observer la foule**

### Mathius

##### 23/04/2023 17:32

##### <https://discord.com/channels/@me/503896082045206528/1099719393564426260>

**Hisako pu donc apercevoir, la jeune femme qu'elle avait aidé, commencer à se mêler à la foule, elle semblait ainsi déjà bien s'intégrer. À force d'observation elle peut voir les allées et venues des Kobolds dans ce petit hameau, et suite à un certain temps d'observation elle peut voir certaines Dryades et d'autres visages familiers se balader sur la place, certains Kistunes et Dryades allant voir les différentes herbes et d'autres le travail des orfèvres Kobolds.**

### MamaBallou

##### 23/04/2023 17:38

##### <https://discord.com/channels/@me/503896082045206528/1099721024485994577>

**La vue de ces allées et venues la fit sourire. Elle s'y mêla encore et remarqua cette fois un petit étal où une vieille dame avait étendu sa marchandise sur un simple tapis. Marchandise qui était constituée pour la plupart d'objets de pacotille. La femme semblait dans le besoin et Hisako eu envi d'acheter pour l'aider. Elle contemplait donc ce que proposait la vieille femme**

### Mathius

##### 23/04/2023 18:20

##### <https://discord.com/channels/@me/503896082045206528/1099731609386745977>

**La vieille femme proposait divers breuvages et quelques ornements relativement anciens. Cette dernière n'affichait alors pas de prix et semblait relativement assoupie.**

### MamaBallou

##### 23/04/2023 19:30

##### <https://discord.com/channels/@me/503896082045206528/1099749171889709217>

**Hisako choisit l'ornement le plus petit et s'adressa à la vieille femme.**

Je vais vous prendre ça.

**La femme releva doucement la tête l'air surprise que quelqu'un lui adresse la parole**

Je vous en propose 30 pièces de bronze. *dit-elle en tendant le montant annoncé*

### Mathius

##### 23/04/2023 21:01

##### <https://discord.com/channels/@me/503896082045206528/1099771943193948190>

**La femme commence alors à s'exprimer en réponse**
Votre monnaie ne m'intéresse guère voyageuse, si vous souhaitez obtenir cet ornement, n'avez vous rien d'autre à me proposer ? *dit-elle alors qu'elle garde une expression relativement neutre*

### MamaBallou

##### 23/04/2023 21:12

##### <https://discord.com/channels/@me/503896082045206528/1099774902455783424>

Autre chose ? *s'etonna-t-elle en rangeant sa monnaie. Puis elle réfléchis un instant* Je peux vous proposer un service dans la limite de mes capacités. Si je suis en mesure de vous aider, je le ferais. Cela vous conviendrait-il ?

### Mathius

##### 23/04/2023 21:22

##### <https://discord.com/channels/@me/503896082045206528/1099777440018083922>

Que pensez vous pouvoir me proposer comme service ? *dit-elle toujours aussi cryptique dans son discours*

### MamaBallou

##### 23/04/2023 21:39

##### <https://discord.com/channels/@me/503896082045206528/1099781708750000261>

*Elle hésita un instant* Disons que je sais convaincre les gens et rester discrète. Alors c'est à vous de me dire ce qu'il vous faut. *dit-elle d'une voix innocente*

### Mathius

##### 23/04/2023 21:48

##### <https://discord.com/channels/@me/503896082045206528/1099783929025802300>

Convaincre ? *dit-elle visiblement intriguée mais intéressée* Pourriez vous me prouver cela en faisant une démonstration sur votre cortège avant que je ne puisse vous demander de le faire ? *toujours aussi cryptique*

### MamaBallou

##### 23/04/2023 21:52

##### <https://discord.com/channels/@me/503896082045206528/1099784941258154085>

*Elle soupira et regarda autour d'elle* Faisons autrement. Je vous rend le service et je n'aurai l'objet qu'après. Ainsi, même si je ne remplis pas ma part du marché, vous n'aurez rien perdu. Par contre, si je réussi, vous me donnerez l'objet.

### Mathius

##### 23/04/2023 22:11

##### <https://discord.com/channels/@me/503896082045206528/1099789662396088560>

En ce cas comment je pourrais savoir que vous ne risquez pas d’aggraver la situation initiale ? *dit-elle méfiante*

### MamaBallou

##### 23/04/2023 22:15

##### <https://discord.com/channels/@me/503896082045206528/1099790762683674834>

*Elle soupira de nouveau* Soit, vous voyez l'homme bien habillé là bas. *commença-t-elle en pointant du doigt de gentilhomme habillé élégamment arpentant le marché* Dans 2 minutes, il va aller vers cet estropié et lui donner 3 pièces d'argent. C'est suffisant comme preuve ?

### Mathius

##### 23/04/2023 22:17

##### <https://discord.com/channels/@me/503896082045206528/1099791126598266942>

Soit, je vous donnerais ceci en échange *dit-elle en pointant une petite breloque qui fait le tier de celle convoité par Hisako*

### MamaBallou

##### 23/04/2023 22:25

##### <https://discord.com/channels/@me/503896082045206528/1099793303408480327>

Marché conclu *conclut-elle en s'élançant vers le gentilhomme*

**Elle slaloma agilement dans la foule et arriva à hauteur du gentilhomme, le salua. Il s'apprêta à partir quand il se figea puis se dirigea vers l'estropié. Il sorti sa bourse et lui tendit alors 3 pièces d'argent. L'homme en face de lui, interloqué, lui adressa un regard étonné mais attrapa l'argent et parti sans demander son reste. Le gentilhomme rangea sa bourse et continua alors son chemin comme si rien ne s'était passé. Le temps que la scène se termine, Hisako était retournée auprès de la vielle dame.**

Alors ? Satisfaite ?

### Mathius

##### 23/04/2023 22:29

##### <https://discord.com/channels/@me/503896082045206528/1099794243339419798>

Surprenant *dit-elle satisfaite et en tendant l'ornement* En ce cas, je vous demanderais de convaincre le gobelin à l'autre bout de la rue afin qu'il modère ses propos à l'encontre des kobolds que nous sommes, cela permettrait au village de se porter un peu mieux en enlevant cette querelle intestine *dit-elle avec un sourire quasi machiavélique*

### MamaBallou

##### 23/04/2023 22:39

##### <https://discord.com/channels/@me/503896082045206528/1099796683094769765>

**Hisako récupera l'ornement puis, réfléchit à peine et hocha la tête avant de s'élancer vers ledit gobelin**

**Elle commença comme pour passer à côté de lui et le bouscula par "mégarde". Son visage commença à exprimé de l'énervement puis se detentit avant de devenir jovial. S'en suivit quelques minutes de conversation. Puis Hisako revint vers la vieille femme**

Un peu plus têtu celui-ci. Je comprends ce que vous vouliez dire *sourit-elle chaleureusement*

### Mathius

##### 23/04/2023 22:43

##### <https://discord.com/channels/@me/503896082045206528/1099797733352685638>

Je ne vais pas vous faire l'affront de vous demandez comment vous avez fait, voici ce que vous attendiez. *dit-elle en tendant l'ornement tant attendu*

### MamaBallou

##### 23/04/2023 22:47

##### <https://discord.com/channels/@me/503896082045206528/1099798649032146975>

Merci Madame. C'était un honneur de faire affaire avec vous *sourit-elle joyeusement*

**Elle la salua et commença à s'éloigner en tenant contre elle ses bonnes affaires de la journée**

### Mathius

##### 23/04/2023 23:10

##### <https://discord.com/channels/@me/503896082045206528/1099804542310293554>

**Pendant tout ce temps Adaram a pû faire quelques emplettes en terme d'herbes médicinales avant de remarquer Hisako se diriger vers un gobelin, il fut surpris dans un premier temps avant de s'approcher de celle-ci et de remarquer une étrange ondulation autour de celle-ci, ne sachant que dire et que faire, il s'éloigne un peu et l'observe discrètement depuis la foule. Il la suit alors à pas feutrés lorsqu'il la voit finir sa transaction avec cette étrange vieille dame.**

### MamaBallou

##### 24/04/2023 00:38

##### <https://discord.com/channels/@me/503896082045206528/1099826693390217319>

**Hisako se dirigea vers les caravanes en bordure du village et s'assit près d'un buisson. Elle commença alors à frotter les ornements qu'elle venait d'acquérir pour juger de leur qualité**

### Mathius

##### 24/04/2023 00:46

##### <https://discord.com/channels/@me/503896082045206528/1099828710942711928>

**Adaram s'installa alors discrètement dans le buisson pour tenter de comprendre le sens des actions d'Hisako**

### MamaBallou

##### 24/04/2023 01:00

##### <https://discord.com/channels/@me/503896082045206528/1099832091480436757>

**Au fur et à mesure que les ornements étaient polis, de jolis motifs apparaissaient sur leurs surfaces. Au centre du premier, se trouvait un topaze bleu clair. Elle l'inspecta et, satisfaite de ce qu'elle y voyait, la posa sur le côté. Pour le second et plus volumineux ornement, ce fut plus long. La couche de crasse était plus épaisse et l'objet avait beaucoup de creux difficiles à atteindre. Au bout de plusieurs longues minutes, elle finit enfin. Au centre de l'ornement, un énorme grenat. En le voyant, les oreilles d'Hisako tressaillirent et sa queue s'agita. Elle ne pu retenir un cri de victoire**

Oui ! Quelle aubaine ! **Elle l'observa encore de plus près et se calma** Pas autant de chance que pour l'autre, mais ça peut s'arranger. *conclue-t-elle*

**Elle regarda le ciel, il était encore tôt et le soleil brillait. Elle commença à ramasser ses affaires et se relever, visiblement décidé à changer d'endroit**

### Mathius

##### 24/04/2023 01:05

##### <https://discord.com/channels/@me/503896082045206528/1099833372815151155>

**Adaram fut intrigué et en découvrant alors ce que cherchait la kitsune il fut déçu et lorsque cette dernière se leva il parti à son tour, bien moins discrètement même si il ne s'en rendu pas compte.**

### MamaBallou

##### 24/04/2023 01:12

##### <https://discord.com/channels/@me/503896082045206528/1099835307228139581>

**Hisako trop heureuse de ses découvertes ne vit ni n'entendit Adaram. Elle se dirigea cette fois en dehors du village vers une grotte que lui avait indiqué "gentillement" par un habitant. Une fois arrivée, elle déblaya toute les branches et végétaux du sol de la grotte, disposa des pierres collées les unes aux autres pour formé un cercle. Elle retira ensuite sa veste pour ne garder que sa légère tunique dans laquelle elle semblait trembler de froid en ce début de Mai. Elle prit soin d'entreposer ses affaires loin du cercle de pierres et s'installa au centre de celui-ci, seulement munit de l'ornement le plus gros et d'une tige de graphite**

### Mathius

##### 24/04/2023 01:28

##### <https://discord.com/channels/@me/503896082045206528/1099839182446276680>

**Adaram commençait alors à retourner vers le campement quand il remarqua qu'Hisako partait quasiment à l'opposé. Cela l'intriga un peu, dans un premier temps il repartait malgré tout vers le campement avant de finalement céder à la tentation de Pandore et de suivre Hisako avant de voir cette dernière au sein de son rituel**

### MamaBallou

##### 24/04/2023 01:36

##### <https://discord.com/channels/@me/503896082045206528/1099841300632707214>

**Hisako resta debout, l'ornement entre les mains. Elle sembla tracer quelque chose sur l'objet, de là où il était Adaram ne pouvait pas voir ce que c'était. Une fois fait, Hisako lança délicatement le graphite vers ses affaires puis se concentra. L'air se mis alors à vibrer et une vague de chaleur émana de la grotte. L'ornement rougoyait et flotait à présent au dessus des mains de la jeune femme. Il faisait si chaud qu'une brindille aurait pu s'enflamer à la moindre étincelle. Hisako commença à avoir chaud également, quelques gouttes de sueur perlèrent sur son visage. Il y eu alors un éclair lumineux. Adaram fut ébloui et lorsqu'il discerna à nouveau le contour des choses, Hisako était allongée par terre, l'ornement non loin d'elle rougeoyait encore et le sol autour était brûlant. Elle ne bougeait pas mais semblait respirer.**

### Mathius

##### 24/04/2023 11:16

##### <https://discord.com/channels/@me/503896082045206528/1099987145306951731>

**Adaram s'approcha inquiet pour celle-ci après l'éclair. Il s'approcha d'elle comme pour prendre son pouls avant de la voir respirer. Rassuré il commenca à retourner dans son buisson comme s'il ne voulait pas se faire attraper**

### MamaBallou

##### 24/04/2023 13:03

##### <https://discord.com/channels/@me/503896082045206528/1100014173586460672>

**Plusieurs minutes passèrent avant qu'Hisako ne reprenne ses esprits et se redresse en se tenant la tête.**

Merde... Un peu trop. *elle attrape l'ornement qui avait eu le temps de refroidir* Bon au moins c'est réussi *sourit-elle doucement*

**Elle se releva doucement et avec difficulté, titubant plusieurs fois et se dirigea vers ses affaires pour ranger et se rhabiller. Elle s'arrêta encore plusieurs fois dans le processus pour reprendre ses esprits et essayé d'être moins sonnée et étourdit. Une fois prête, elle se dirigea vers la sortie de la grotte, se tenant toujours la tête.**

### Mathius

##### 24/04/2023 16:10

##### <https://discord.com/channels/@me/503896082045206528/1100061234138726411>

**Adaram prenait alors plus de risque en poursuivant sa filature afin d'observer de plus près le bâton ainsi formé**//Quel bâton?

### MamaBallou

##### 24/04/2023 16:13

##### <https://discord.com/channels/@me/503896082045206528/1100061841465557063>

**Hisako s'étira et capta un craquement de branche non loin. Ses oreilles s'orientèrent, à l'affût du moindre son. Et elle regarda autour d'elle.**

### Mathius

##### 24/04/2023 16:19

##### <https://discord.com/channels/@me/503896082045206528/1100063377872654366>

**Adaram est alors caché comme il peut derrière un arbre le cœur battant à tout allure en ayant du mal à calmer sa respiration par la crainte de se faire attraper**

### MamaBallou

##### 24/04/2023 16:21

##### <https://discord.com/channels/@me/503896082045206528/1100063938793709680>

**Hisako sentait que quelque chose n'allait pas mais finit par s'en aller en pressant le pas pour rejoindre les caravanes, d'autant qu'il commençait à se faire tard**

### Mathius

##### 24/04/2023 16:27

##### <https://discord.com/channels/@me/503896082045206528/1100065474739441705>

**Adaram se calme enfin et décide de rentrer sagement jusqu'à sa roulotte.**

### Mathius

##### 24/04/2023 16:43

##### <https://discord.com/channels/@me/503896082045206528/1100115519253782701>

**La caravane commence alors à reprendre la route en faisant ses adieux au village, la tension s’apaise progressivement durant les jours suivants la reprise du trajet notamment depuis le départ de la protégée des Kitsunes. La caravane s'enfonça alors un peu plus profondément dans la forêt**

### MamaBallou

##### 24/04/2023 20:03

##### <https://discord.com/channels/@me/503896082045206528/1100119931644825646>

**Hisako passa son temps sur le toit de sa caravane à observer, allongée, la cimes des arbres. Elle avait l'air fatiguée et de grosses cernes bordaient ses yeux. Elle profitait de ce calme pour écouter les conversations alentours**

### Mathius

##### 24/04/2023 20:16

##### <https://discord.com/channels/@me/503896082045206528/1100123138542538752>

**Suite au calme imposé par le départ de la protégée d'Hisako, Kitsune et Dryades se mèlent à nouveau et Hisako peut entendre par moment des jeunes Dryades et des jeunes Kitsunes jouer ensemble, divers adultes évoquer le repas du soir ou forcément l'une des personnes que nous suivons comme Adaram qui discute des essences qui pousse aux environs**//Le pauvre boug sont seul très de caractère c'est l'encens.

### MamaBallou

##### 24/04/2023 21:08

##### <https://discord.com/channels/@me/503896082045206528/1100136137143627867>

**Elle se laissa bercer par le roulis et le rythme des conversation. Elle somnola jusqu'à ce que les caravanes s'arrêtent... au milieu de la route. Elle se redressa pour voir de quoi il retournait et aperçut un roulotte arrêtée devant eux.**

### Mathius

##### 24/04/2023 21:25

##### <https://discord.com/channels/@me/503896082045206528/1100140474636959965>

**Cette roulotte semblait bien usée par le temps mais aucun bruit ne semblait s'en échapper.**

### MamaBallou

##### 24/04/2023 21:34

##### <https://discord.com/channels/@me/503896082045206528/1100142854208229396>

**Les membres de chacune des caravanes commencèrent alors à regarder aux alentours pour trouver le propriétaire de la roulotte, sans résultat. À l'intérieur, rien d'autre que des caisses vides et l'une des roues était endommagée. Ils arrivèrent donc à la conclusion que le propriétaire avait dû l'abandonner là.**

### Mathius

##### 24/04/2023 22:01

##### <https://discord.com/channels/@me/503896082045206528/1100149462145454160>

**Hisako semblait malgré tout intriguée par la caravane.**

### MamaBallou

##### 24/04/2023 22:39

##### <https://discord.com/channels/@me/503896082045206528/1100159200585330769>

**Elle s'approcha donc de la roulotte et regarda à l'intérieur et huma l'air.**

### Mathius

##### 24/04/2023 22:48

##### <https://discord.com/channels/@me/503896082045206528/1100161359771086848>

**L'air sentait la tourbe et la roulotte semblait sens dessus dessous.**

### MamaBallou

##### 24/04/2023 22:52

##### <https://discord.com/channels/@me/503896082045206528/1100162252222496888>

**Elle regarda plus en détail les caisses mais n'y trouva rien.**

### Mathius

##### 24/04/2023 23:29

##### <https://discord.com/channels/@me/503896082045206528/1100171775997919313>

**Elle pû fouiller un peu plus en détail le reste et trouva visiblement un vieux carnet dans une étagère en ruine.**

### MamaBallou

##### 24/04/2023 23:39

##### <https://discord.com/channels/@me/503896082045206528/1100174310628413492>

**Elle prit le carnet et le fourra dans sa poche. Elle frappa le sol de la roulotte pour être sur qu'il n'y ait pas de double fond puis retourna vers sa caravane, laissant les autres réparer la roue de la roulotte et l'ajouter au cortège. Elle se remit sur le toit de sa caravane et commença à feuilleter ledit carnet.**

### Mathius

##### 24/04/2023 23:46

##### <https://discord.com/channels/@me/503896082045206528/1100175936990752839>

**La langue ne lui était pas étrangère sans savoir ce qui était dépeint, probablement un alphabet assez proche cependant elle peut rapidement parcourir les divers schémas qui ornèrent le carnet, ceux-ci semblaient représenter des moulins et des roues à aubes.**

### MamaBallou

##### 24/04/2023 23:48

##### <https://discord.com/channels/@me/503896082045206528/1100176526726672445>

**Elle alla demander conseil à l'ancien des kitsunes pour voir si celui-ci reconnaissait l'alphabet**

### Mathius

##### 24/04/2023 23:53

##### <https://discord.com/channels/@me/503896082045206528/1100177712099885167>

**Il mit un temps certain en cherchant avant de conclure en hésitant** Probablement du gobelin ou du wukong *(Une sorte de petit chimpanzé humanoïde)*

### MamaBallou

##### 24/04/2023 23:54

##### <https://discord.com/channels/@me/503896082045206528/1100178081823596595>

Merci *dit-elle avec un salut respectueux de la tête.* Vous sauriez le traduire ? *demanda-t-elle hésitante.*

### Mathius

##### 24/04/2023 23:56

##### <https://discord.com/channels/@me/503896082045206528/1100178540336525422>

Malheureusement non *dit-il d'un air désolé* Notre clan n'a pas croisé ces deux peuplades depuis des décennies, peut-être que d'autres peuplades pourront aider ?
//Qu'est ce qui raconte le vieux, il est complétement sénile ou quoi? Ils ont discuté avec des gobelins au village littéralement HIER

### MamaBallou

##### 25/04/2023 00:02

##### <https://discord.com/channels/@me/503896082045206528/1100180066123665478>

Merci vénérable ancien. *ajoute-t-elle en le saluant bien bas*

**Elle sortit de la caravane de l'ancien et le cortège repartait déjà. Elle monta de nouveau sur le toit de sa caravane et repéra de loin le seul dryade qu'elle connaissait : Adaram. Elle sauta à terre et commença à slalomer entre les caravanes en mouvement pour le rejoindre, le carnet à la main/**

### Mathius

##### 25/04/2023 10:38

##### <https://discord.com/channels/@me/503896082045206528/1100340016842014730>

**Celui-ci fut surpris de l'arrivée de la jeune Kitsune, et s'exprima alors dans ces termes**

Bonjour, Hisako ? *dit-il d'un ton relativement hésitant comme s'il n'était pas sûr du nom*

### MamaBallou

##### 25/04/2023 13:24

##### <https://discord.com/channels/@me/503896082045206528/1100381888763412490>

Bonjour, tu aurais une minute j'ai une question à te poser. Ça ne sera pas long. *répondit-elle d'un ton le plus neutre possible bien qu'intimidée à l'idée d'être au milieu de toutes ces dryades*

### Mathius

##### 25/04/2023 23:08

##### <https://discord.com/channels/@me/503896082045206528/1100528689529237534>

**Il semble relativement surpris et lui répondit** Ou oui ? *dit-il en semblant disposé à la suivre ou à l'écouter*

### MamaBallou

##### 26/04/2023 00:11

##### <https://discord.com/channels/@me/503896082045206528/1100544556077940746>

**Elle se decala un peu en retrait du cortège et lui tendit le carnet**

Est-ce que tu saurais me dire ce que ça signifie ? *demanda-t-elle*

### Mathius

##### 26/04/2023 01:01

##### <https://discord.com/channels/@me/503896082045206528/1100557115585724416>

**Il sembla surpris dans un premier temps avant de tenter de lire le carnet sur lequel il semble buter un mot sur deux avant de finalement le refermer** Je je ne connais pas cet alphabet *Il semblait déçu et curieux à la fois* où l'a tu trouvé ? *ajoute-t-il*

### MamaBallou

##### 26/04/2023 01:27

##### <https://discord.com/channels/@me/503896082045206528/1100563765503741953>

**Elle reprit le carnet. Et hésita avant de répondre**

... dans la roulotte arrêtée au milieu de la voie. *elle décida d'insister* Notre ancien pense que ce serait du gobelin ou du wukong. Tu connais quelqu'un de ton clan qui connaîtrait un de ces dialectes ?

### Mathius

##### 26/04/2023 02:13

##### <https://discord.com/channels/@me/503896082045206528/1100575350058143814>

**Il sembla hésiter un instant avant de secouer la tête à deux reprises** Non pas nos prêtresses, nos guérisseurs non plus *puis en semblant avoir trouvé quelque chose* Peut-être l'impie ? *dit-il pas sûr de lui et en pointant une autre roulotte en retrait du convoi*

### MamaBallou

##### 26/04/2023 02:26

##### <https://discord.com/channels/@me/503896082045206528/1100578560307101796>

Génial, allons lui demander alors. Tu me présentes et je lui demande. *dit-elle enjouée en commençant à se diriger vers la roulotte qu'il lui a indiqué*

### Mathius

##### 26/04/2023 08:38

##### <https://discord.com/channels/@me/503896082045206528/1100672220549890149>

**Adaram la suit en hochant timidement la tête comme si il était terrifié à l'idée d'aller voir 'l'impie'**

### MamaBallou

##### 26/04/2023 15:30

##### <https://discord.com/channels/@me/503896082045206528/1100775814590627881>

**Ils arrivent alors tous les deux devant la caravane et Hisako se tourne vers Adaram**

À toi l'honneur

### Mathius

##### 26/04/2023 16:18

##### <https://discord.com/channels/@me/503896082045206528/1100788101250547785>

**Il toque timidement avant d'attendre une réponse relativement stressée**

### MamaBallou

##### 26/04/2023 16:20

##### <https://discord.com/channels/@me/503896082045206528/1100788477462859856>

**De longues secondes passent. Hisako regarde Adaram pour lui demander si ça va être long mais la porte s'ouvre**

### Mathius

##### 26/04/2023 18:58

##### <https://discord.com/channels/@me/503896082045206528/1100828341340221500>

**La porte s'ouvre en grinçant, une silhouette se tient alors cachée dans la maison avant de commencer à prendre la parole avec une voix chevrotante**

Qu-que me vou-voulez vous ? *dit la silhouette sans parvenir à fixer un genre sur cette voix*

### MamaBallou

##### 26/04/2023 19:14

##### <https://discord.com/channels/@me/503896082045206528/1100832346455953529>

**Hisako prit une grande inspiration pour se donner du courage et dit d'une traite d'une voix posée**

Bonjour, Adaram m'a dit que vous pourriez peut-être m'aider. J'ai trouvé un carnet mais je ne sais pas ce qui est écrit. Ce serait du gobelin ou du wukong peut-être. Vous sauriez le traduire ?

### Mathius

##### 26/04/2023 19:19

##### <https://discord.com/channels/@me/503896082045206528/1100833606366150676>

**La silhouette semble baragouiner un message avant de s'approcher de prendre le carnet et marmonner un faible 'suivez moi' alors qu'elle s'installe dans sa roulotte que l'on qualifierait de roulotte de hobbit et plus précisément sur une table ~~avec du saucisson de sorti~~**

### MamaBallou

##### 26/04/2023 19:22

##### <https://discord.com/channels/@me/503896082045206528/1100834180784455791>

**Hisako lance un regard à Adaram puis entre dans sa caravane en faisant attention de ne rien faire tomber et s'assoit de l'autre côté de la table.**

### Mathius

##### 26/04/2023 19:28

##### <https://discord.com/channels/@me/503896082045206528/1100835932648775694>

**Adaram la suit prenant alors toutes ses précautions en entrant dans cette roulotte et s'installe alors à côté d'Hisako même si cette dernière peut voir des regards intrigués voir 'mauvais' entre Adaram et la silhouette**

### MamaBallou

##### 26/04/2023 19:43

##### <https://discord.com/channels/@me/503896082045206528/1100839471869599855>

**Maintenant qu'ils étaient assis et que leurs yeux s'accoutumaient à l'obscurité, les 2 comparses distinguent mieux la silhouette qui les accueillait. Il s'agissait d'une dryade aux cheveux en dreads locks montés en chignon complexe. Elle avait le regard fatigué et les joues légèrement émaciées. Hisako eu un regard interrogateur sur le comportement de Adaram mais décida de ne pas relever, estimant qu'il n'était pas de son ressors de s'immiscer dans les affaires des dryades. Elle choisit donc de simplement relancer la conversation.**

Merci, de nous accueillir madame. *commença-t-elle respectueusement* J'espère que vous saurez déchiffrer le carnet, je ne saurais pas à qui demander d'autre. *essaya-t-elle de plaisanter pour détendre l'atmosphère*

### Mathius

##### 26/04/2023 19:57

##### <https://discord.com/channels/@me/503896082045206528/1100843152287481916>

**La dryade commence alors à parcourir le carnet alors qu'Adaram cherche visiblement ses mots et qu'Hisako et Adaram entendent la dryade marmonnant en continuant de feuilleter**

### MamaBallou

##### 26/04/2023 19:58

##### <https://discord.com/channels/@me/503896082045206528/1100843409251512391>

**Hisako lance un regard décontenancé et perdu à Adaram. L'interrogeant du regard**

### Mathius

##### 26/04/2023 21:40

##### <https://discord.com/channels/@me/503896082045206528/1100869030014881863>

**Adaram se sentant partiellement acculé se force à se lancer**

Est-ce que tu trouves quelque chose, Inashi ? *dit-il avec hésitation alors que cette dernière ne semble pas réagir de prime abord*

### MamaBallou

##### 26/04/2023 22:14

##### <https://discord.com/channels/@me/503896082045206528/1100877637607497758>

**Inashi, comme l'avait appelé Adaram la regarda de travers avant de replonger son regard sur le carnet et de conclure par l'affirmative sans lui adresser le moindre regard.**

### Mathius

##### 26/04/2023 00:22

##### <https://discord.com/channels/@me/503896082045206528/1100909776076025866>

**Inashi en fermant alors le carnet regarda Hisako sans prêter la moindre attention à Adaram et lui répondit**

Ceci est un livre de note d'un wunkong en effet et d'après ses notes il semble suivre notre trajet également *dit-elle sans ajouter plus de détail*

### MamaBallou

##### 27/04/2023 00:24

##### <https://discord.com/channels/@me/503896082045206528/1100910326809112707>

**Hisako attendit quelques instant quelle ajoute quelque chose mais rien ne vint.**

Comment ça il suit notre trajet ? *pause* Il a, avant nous, il y quelques jours, suivi le même chemin que nous empruntons aujourd'hui ?

### Mathius

##### 27/04/2023 00:25

##### <https://discord.com/channels/@me/503896082045206528/1100910660994478201>

En quelque sorte *dit-elle en tendant le carnet*

### MamaBallou

##### 27/04/2023 00:26

##### <https://discord.com/channels/@me/503896082045206528/1100910925034307684>

**Hisako essaya encore**

C'est à dire ? Vous pourriez être plus précise ? *elle commençait à s'impatienter*

### Mathius

##### 27/04/2023 00:29

##### <https://discord.com/channels/@me/503896082045206528/1100911630449119394>

Il décrit notre trajet et semble le suivre vu les descriptions mais il n'y a pas de date dans son carnet *dit-elle comme pour clore la conversation alors qu'elle se lève*

### MamaBallou

##### 27/04/2023 00:31

##### <https://discord.com/channels/@me/503896082045206528/1100912024982143027>

**Hisako la regarda sans comprendre**

Et où s'arrête la description de ce voyage ?

### Mathius

##### 27/04/2023 00:33

##### <https://discord.com/channels/@me/503896082045206528/1100912618417438740>

Au dernier village que nous avons visité mais il n'est nul part mentionné des Dryades ou d'autres peuplades *ajoute-t-elle d'un ton relativement sec*

### MamaBallou

##### 27/04/2023 00:35

##### <https://discord.com/channels/@me/503896082045206528/1100912974400598096>

Autre chose d'important que je devrais savoir ? **insista-t-elle déterminé**

### Mathius

##### 27/04/2023 00:37

##### <https://discord.com/channels/@me/503896082045206528/1100913611267899452>

J'en doute *acheva-t-elle avant de prononcer quelque chose en elfique qui sembla terrifier Adaram. Ce dernier qui décida alors de partir en incitant Hisako à la suivre*

### MamaBallou

##### 27/04/2023 00:41

##### <https://discord.com/channels/@me/503896082045206528/1100914513806622821>

Très bien **trancha Hisako**

**Elle ne comprenait pas l'empressement soudain de Adaram mais se leva et ramassa le carnet**

Merci encore pour votre aide Madame Inashi. **conclut elle en s'inclinant légèrement** Je ne vous dérangerais pas plus longtemps dans ce cas.

**Elle commença a se diriger vers la porte**

### Mathius

##### 27/04/2023 11:13

##### <https://discord.com/channels/@me/503896082045206528/1101073621235015691>

**La vieille dame les laissa sortir avant de fermer sa roulotte et les deux comparses peuvent alors l'entendre verrouiller sa porte**

### MamaBallou

##### 27/04/2023 14:21

##### <https://discord.com/channels/@me/503896082045206528/1101120863157370992>

Et bien au moins j'aurais eu mes réponses **essaya de positiver Hisako** Par contre, tu peux m'expliquer ce qu'il s'est passé à la fin pour que tu paniques comme ça. **demanda-t-elle à Adaram alors qu'elle peinait à le suivre tant il s'éloignait rapidement de la caravane**

### Mathius

##### 27/04/2023 16:33

##### <https://discord.com/channels/@me/503896082045206528/1101154168443519111>

C'est particulier *avant d'ajouter de manière peu intelligible* tu ne va peut-être pas saisir *alors qu'il ne calmait pas son pas pressé vers le gros de la caravane*

### MamaBallou

##### 27/04/2023 17:06

##### <https://discord.com/channels/@me/503896082045206528/1101162561682014248>

**Elle le rattrape par l'épaule pour qu'il ralentisse**

Je préfère que tu me laisses juger par moi même si je comprend ou pas quelque chose. Explique. **dit-elle d'une voix ferme**

### Mathius

##### 27/04/2023 19:12

##### <https://discord.com/channels/@me/503896082045206528/1101194283509358682>

**Il se calle à son rythme avant de répondre un peu honteux**

C'est une impie, une Kipasht, elle a renié les divinités auxquelles nous avons confié notre groupe.

### MamaBallou

##### 27/04/2023 19:19

##### <https://discord.com/channels/@me/503896082045206528/1101195837020852254>

**Elle ne dis rien pendant un instant** Je vois. Et pourquoi est-ce que tu as eu l'air de prendre peur à la fin, juste avant qu'on s'en aille ?

### Mathius

##### 27/04/2023 19:26

##### <https://discord.com/channels/@me/503896082045206528/1101197685198954607>

Elle semblait particulièrement agacée et des rumeurs courent sur des malédictions qu'elle pourrait provoquer d'une certaine façon

### MamaBallou

##### 27/04/2023 19:28

##### <https://discord.com/channels/@me/503896082045206528/1101198310519357480>

**Hisako fronça les sourcils**

Des malédictions ?! Tu aurais pu me prévenir avant de m'y emmener ! **s'indigna-t-elle**

### Mathius

##### 27/04/2023 19:41

##### <https://discord.com/channels/@me/503896082045206528/1101201368267636868>

J-je je doute qu'elle nous en ai lancé une *dit-il surpris de la réaction d'Hisako*

### MamaBallou

##### 27/04/2023 19:46

##### <https://discord.com/channels/@me/503896082045206528/1101202650487345212>

*soupire* Bon, maintenant de toutes façons c'est trop tard... Qu'est-ce que tu penses de ce qu'elle a dit ? **changea-t-elle de sujet**

### Mathius

##### 27/04/2023 19:54

##### <https://discord.com/channels/@me/503896082045206528/1101204732892164237>

Qu'un wukong est probablement à proximité de notre caravane ou qu'un cadavre de wukong n'est pas très loin *dit-il sombre*

### MamaBallou

##### 27/04/2023 19:56

##### <https://discord.com/channels/@me/503896082045206528/1101205345658994811>

Hm... C'est quand même bizarre qu'il ne soit resté que le carnet. Sa roulotte était complètement vide à part ça. Peut-être qu'il s'est fait embusquer ? **hypothèsa-t-elle**

### Mathius

##### 27/04/2023 19:58

##### <https://discord.com/channels/@me/503896082045206528/1101205865383608330>

Peut-être, il faudrait peut-être chercher aux alentours ? *hazarda-t-il*

### MamaBallou

##### 27/04/2023 20:02

##### <https://discord.com/channels/@me/503896082045206528/1101206830438428803>

Bonne idée, mais je pense qu'on s'est trop éloigné de là où on a trouvé la roulotte maintenant. Tout ce qu'il y a autours **regarde autour** c'est des arbres, des feuilles et des caravanes... **ironisa-t-elle**

### Mathius

##### 27/04/2023 20:28

##### <https://discord.com/channels/@me/503896082045206528/1101213348609151137>

On peut tenter de l'évoquer aux autres *dit-il maladroitement ce qui donne une autre impression à Hisako*

### MamaBallou

##### 27/04/2023 20:33

##### <https://discord.com/channels/@me/503896082045206528/1101214455251095743>

Je pense que vu l'atmosphère? ils se doutent déjà qu'il faut se méfiER µ*dit-elle en pointant les regards sérieux autours d'euxµ* Ce n'est jamais bon signe de trouver une roulotte abandonnée. Encore moins quand elle est vide.

**À peine finissait-elle sa phrase qu'il se mit à pleuvoir. D'abord 3 goutes, puis tout une averse.**

Ça c'est la tuile. *dit-elle simplement en essayant de se couvrir la tête*

### Mathius

##### 27/04/2023 20:44

##### <https://discord.com/channels/@me/503896082045206528/1101217424923185203>

*L'imite en marmonnant* Oui, l'ambiance ne va pas aller en se calmant malheureusement

### MamaBallou

##### 27/04/2023 21:12

##### <https://discord.com/channels/@me/503896082045206528/1101224347001176185>

Et maintenant on entendra plus rien... **bougona-t-elle**

**La fin de la journée se déroula sous la pluie et avec monotonie. Adaram et Hisako avaient chacun regagné leur caravane pour se mettre à l'abris. Le jour suivant et celui d'après se ressemblaient aussi. Cela avait pour effet de rendre maussade les kitsunes tandis que les dryades semblaient de leur côté, ne pas s'en incommoder. En début d'après-midi du surlendemain de la découverte du carnet, le ciel se dégagea enfin. Hisako ressortit enfin de la caravane qu'elle avait à peine quittée mais à peine eut-elle sortie la tête que le convoi s'arrêta pour la deuxième fois en 3 jours au milieu de la voie. Cette fois ci, un arbre était tombé sur la route, un énorme hêtre. La pluie et le vent l'auront sans doute fait tomber là la nuit précédente. De plus, il était impossible de le contourner tant la forêt était dense. Le convoi ne passerait pas. Il faudrait donc plusieurs heures avant que la route soit dégagée de nouveau. Autant dire qu'ils allaient passer la nuit là.**

**Maussade, le convoi prit ses quartiers en lisière de la forêt, le long de la route, tandis que les plus costauds s'armaient de hache, de scies et de cordes pour dégager le mastodonte.**

**Hisako agacée sortit donc se dégourdir les jambes et profiter du beau temps retrouvé, à défaut d'autre chose.**

### Mathius

##### 27/04/2023 21:31

##### <https://discord.com/channels/@me/503896082045206528/1101229191631998976>

**Adaram se dégourdissait également les pattes en se déplaçant dans la forêt comme à la recherche d'une cause à cet arbre effondré, ce qui donnait un spectacle relativement surprenant où on pouvait voir la dryade sauter un peu partout alors qu'un arbre était au sol**

### MamaBallou

##### 27/04/2023 21:38

##### <https://discord.com/channels/@me/503896082045206528/1101230959321436171>

**Adaram pourra remarquer que le sol est particulièrement meuble à cause de la pluie et que les racines de l'arbre semblent avoir été fragilisées par des parasites. Rien qui sorte de l'ordinaire.**

**Hisako de son côté ne trouvait rien de folichon non plus. Et s'enfonça un peu plus dans la forêt**

### Mathius

##### 27/04/2023 21:40

##### <https://discord.com/channels/@me/503896082045206528/1101231408892092456>

**Adaram commença alors à s'aventurer un peu plus en profondeur comme s'il cherchait un lieu, peut-être un lieu pour un rituel similaire à celui de la dernière fois**

**Pendant ce temps Hisako peut voir d'autres buissons en piteux états.**

### MamaBallou

##### 27/04/2023 21:43

##### <https://discord.com/channels/@me/503896082045206528/1101232057113399426>

**Adaram pourra trouver un lieu qui parait adéquat, mais en s'approchant davantage, il remarque des altérations atypiques de ce type de forêt, ainsi que des traces de passage.**

**Hisako s'approche des buissons pour essayer de voir s'il s'agirait d'un animal intéressant pour le soir puisque cette fois elle a pris un arc et quelques flèches.**

### Mathius

##### 27/04/2023 21:46

##### <https://discord.com/channels/@me/503896082045206528/1101232867192885350>

**Adaram commença à regarder sans savoir s'il s'agissait d'empreintes d'animaux ou d'autre chose**

**Pendant ce temps, Hisako, commence à voir une piste de sa proie qui semble s'enfoncer dans la forêt comme si cette dernière sautait de buissons en buissons**

### MamaBallou

##### 27/04/2023 21:51

##### <https://discord.com/channels/@me/503896082045206528/1101234184946716763>

**Adaram conclura que c'est tout sauf animal. Et que cela vient de plus loin dans la forêt. Étonnamment, les empruntes semblent presques effacées. Il sait que s'il fait demi tour, elles auront disparut le temps qu'il revienne.**

**Hisako avance à pas feutrés et prend soin d'être sous le vent pour ne pas effrayer son potentiel diner**

### Mathius

##### 27/04/2023 21:54

##### <https://discord.com/channels/@me/503896082045206528/1101235037275426868>

**Il commence alors à les suivre prudemment en s'aventurant beaucoup plus loin que prévu dans la forêt, l'exploration commençant alors à s'étendre sur une grande période**

**Hisako peut alors trouver le premier indice sur son diner étant des poils roux jonchant le sol**

### MamaBallou

##### 27/04/2023 22:01

##### <https://discord.com/channels/@me/503896082045206528/1101236693698351125>

**Adaram sent alors dans l'air l'odeur d'un feu. Sûrement un feu de camps. Il aperçoit alors un campement et des gens vêtu de capuches couvrant leur visage. Ils ont 2 chevaux attachés à un arbre plus loin et une cariole chargée.**

**Elle relève quelques poils et suit la piste, pressant légèrement le pas pour ne pas perdre sa proie. Elle sent qu'elle n'est plus très loin**

### Mathius

##### 27/04/2023 22:07

##### <https://discord.com/channels/@me/503896082045206528/1101238188057567283>

**Adaram se cache alors et semble épier la conversation des personnes se reposant au sein de ce feu de camp**

**Elle commence alors à trouver un amas de poils comme si c'était la tanière de l'animal, alors que ce petit renard semble partir à ce moment là**

### MamaBallou

##### 27/04/2023 22:16

##### <https://discord.com/channels/@me/503896082045206528/1101240495973355622>

**Il peut alors entendre la silhouette la plus proche de lui :**

- J'en peut plus, combien de temps faudra-t-il encore attendre ? **dit-il impatient**
- Ce soir ce sera l'heure, la pluie a cessée mais le vent fera l'affaire. **répond l'autre forme de l'autre côté du feu**
- Je comprend pas ce qui le pousse à presser les choses comme ça.
- On te demande pas de comprendre d'ailleurs **plaisanta la forme**
- Il en met du temps à pisser l'autre. **lança le premier**

**Le temps qu'Adaram ait eu le temps de voir où était le troisième, l'obscurité l'avait enveloppée**

**Hisako rangea alors son arc dépitée. Elle ne tuerait pas de renard. Elle commença alors à revenir sur ses pas gardant tout de même espoir qu'il y ai un autre gibier dans les environs**

### Mathius

##### 27/04/2023 22:25

##### <https://discord.com/channels/@me/503896082045206528/1101242789980803191>

**Adaram alors assommé, semble percevoir encore des bribes de conversation avant de s'effondrer finalement à même le sol à la merci des trois 'brigands'**

**Hisako pour le moment ne trouvait grand chose d'autre alors que l'obscurité révélait d'autres proies**

### MamaBallou

##### 27/04/2023 22:35

##### <https://discord.com/channels/@me/503896082045206528/1101245280906649600>

**Il entendra une troisième voix**

- C'est pas le bon, mais ça peut toujours servir.

**Puis il perdit conscience**

**Elle continua sa traque bien que la nuit arrive à grand pas, trop frustrée de ne rien avoir à se mettre sous la dent**

### Mathius

##### 27/04/2023 22:42

##### <https://discord.com/channels/@me/503896082045206528/1101247062814761100>

**Elle trouva alors les traces de ce qu'elle identifia comme une biche et décida de la chasser malgré l'heure tardive**

### MamaBallou

##### 27/04/2023 22:46

##### <https://discord.com/channels/@me/503896082045206528/1101247929353769091>

**Adaram est attaché, bâillonné et chargé à l'arrière de la cariole. Ses ravisseurs semblent ravis mais ne lèvent pas le camp pour autant**

**Hisako avance furtivement bien que plus difficilement à cause de l'obscurité. Et essaye de rattraper la biche**

### Mathius

##### 27/04/2023 22:51

##### <https://discord.com/channels/@me/503896082045206528/1101249261410197614>

**Il semble paniqué ce qui transparait dans les yeux alors qu'il regarde interdit ses ravisseurs**

**Hisako arrive malgré plusieurs dizaines de minutes de poursuite à être à portée d'attaque de la biche**

### MamaBallou

##### 27/04/2023 22:55

##### <https://discord.com/channels/@me/503896082045206528/1101250201185947669>

**Les ravisseurs parlent à voix basse et Adaram n'entend qu'un chuchotement lointain sans distinguer de mots.**

**Elle encoche lentement une flèche et se prépare à tirer. Elle retient sa respiration et la flèche part. Manqué !**

Merde ! **peste-t-elle alors que la biche s'enfuit et qu'elle essaye de la rattraper**

### Mathius

##### 27/04/2023 22:59

##### <https://discord.com/channels/@me/503896082045206528/1101251419954233485>

**Adaram tente alors de se focaliser sur la seule chose qu'il peut percevoir, la forêt, comme pour tenter de créer la mousse de la dernière fois**

**Alors qu'elle tente de la rattraper elle voit la biche, cette conne, marquer un arrêt quelques dizaines de mètres plus loin**

### MamaBallou

##### 27/04/2023 23:03

##### <https://discord.com/channels/@me/503896082045206528/1101252368542212207>

**Il réussit percevoir une présence dans la forêt, non loin d'une biche, il y a un bipède. Cependant aucune mousse ne se forme pour l'instant**

**Hisako ralentit son rythme cardiaque et encoche de nouveau une flèche**

### Mathius

##### 27/04/2023 23:05

##### <https://discord.com/channels/@me/503896082045206528/1101252864564789371>

**Il poursuit avec peine sans pouvoir émettre le moindre son, le baillon placé par ses kidnappeurs l'entravant de toute possibilité d'émettre le moindre son**

**Elle parvient à toucher la biche sans soucis, cette débile semblant passionnée par autre chose**

### MamaBallou

##### 27/04/2023 23:09

##### <https://discord.com/channels/@me/503896082045206528/1101253709452820500>

**Adaram parvient à former un petit monticule qu'il peut à présent faire glisser sur le sol à sa guise**

**Hisako décoche une deuxième flèche pour achever la bête. Trop concentrée pour remarquer l'attitude étrange de l'animal**

### Mathius

##### 27/04/2023 23:15

##### <https://discord.com/channels/@me/503896082045206528/1101255288872513608>

**Il semble jouer avec et butter contre divers obstacles comme pour se faire une sorte de carte mentale de la zone, même si cela lui demande une concentration monstrueuse**

**L'animal lâche alors un dernier soupir qui semble faire réagir les kidnappeurs**

Qu'est-ce que ? *dit un premier*

C'est juste une biche *dit alors le second*

### MamaBallou

##### 27/04/2023 23:20

##### <https://discord.com/channels/@me/503896082045206528/1101256543908937909>

**Il parvient à trouver divers terriers et arbres mais guère plus. Il sent par contre que le bipède est tout proche et que la biche est morte.**

**Hisako perçois la voix des personnes et s'accroupit. Elle se rapproche de la biche et récupère les flèches, malheureusement l'une d'elle s'est brisée quand la biche est tombée. Elle commence à attacher les pattes de l'animal pour le charger sur son dos. Et reste alerte. Elle a entendu des voix et il n'est pas question qu'on lui vole son repas.**

### Mathius

##### 27/04/2023 23:26

##### <https://discord.com/channels/@me/503896082045206528/1101258022707273879>

**Adaram tente alors d'approcher la mousse du bipède afin de tenter de comprendrTOe ce qui se passe**

**Hisako peut alors sentir une sorte de mousse comme la bloquer alors qu'elle tente de filer**

### MamaBallou

##### 27/04/2023 23:29

##### <https://discord.com/channels/@me/503896082045206528/1101258807763550279>

**Il perçoit alors plus de détails sur le bipède. C'est une kitsune et il reconnait sa posture. Il l'a déjà vu. Elle est du convoi. C'est Hisako !**

**Elle manque de s'étaler de tout son long et se baisse à nouveau pour voir où elle s'est accrochée les pieds. Elle ne voit rien d'inhabituel qui ne devrait pas être dans une forêt mais elle sent qu'elle est toujours coincée. Elle essaye donc de tater avec ses mains pour trouver l'origine du problème.**

### Mathius

##### 27/04/2023 23:34

##### <https://discord.com/channels/@me/503896082045206528/1101260041866194996>

**Fort de cette information, il semble avoir du mal à réprimer un tel espoir. Il s'agite alors en tentant de la tirer avec la mousse qu'il peut alors que ses gardiens réagissent**
Pourquoi il s'agite ? *dit le troisième*
Je m'en occupe *dit alors le second*

### Mathius

##### 27/04/2023 23:34

##### <https://discord.com/channels/@me/503896082045206528/1101260211781640224>

**Hisako quand a elle sent la mousse l'envahir avant de se sembler comme tirer vers les voix qu'elle entends et qu'elle avait entendu**

### MamaBallou

##### 27/04/2023 23:42

##### <https://discord.com/channels/@me/503896082045206528/1101261999805046896>

**Le second qui avait parlé s'approcha d'Adaram et se planta devant lui. Adaram trop concentré à tirer Hisako par ici ne l'entendit pas venir. Par contre, il sentit distinctement un poing robuste s'abattre sur sa figure et rompre sa concentration.**

- Arrête de t'agiter si tu veux pas un autre rappel à l'ordre *le gronda-t-il*

**Elle est tentée de retirer vivement sa main, dégoutée par cette sensation surprenante. Puis, elle se souvient de la mousse lors de la fête du début de la saison des récoltes. Cette petite mousse animée était très étonnante. Elle cacha la biche morte dans un coin et s'avança dans la direction indiquée. La petite mousse ne semblait plus bouger mais elle entendait les voix non loin. Elle s'abrita derrière l'ombre d'un arbre pour essayer de comprendre pourquoi le monticule moussu l'avait dirigé par là.**

### Mathius

##### 27/04/2023 23:50

##### <https://discord.com/channels/@me/503896082045206528/1101264034055073883>

**Il semblait terrorisé alors qu'il n'osait plus bouger pour l'instant, ni faire de bruit**

**Cette dernière peut alors voir une forme s'eloigner d'une sorte de malle vers deux autres comparses en marmonnant**

### MamaBallou

##### 27/04/2023 23:52

##### <https://discord.com/channels/@me/503896082045206528/1101264713481977886>

**Il n'entend plus rien que les bruits de la forêt, le crépitement du feu et la voix mauvaise des silhouettes.**

**Hisako essaye de tendre l'oreille pour comprendre de quoi il retourne.**

### Mathius

##### 27/04/2023 23:59

##### <https://discord.com/channels/@me/503896082045206528/1101266275453382787>

**Fichu pour fichu, il décida de tenter de reprendre le contrôle de la mousse, chose qu'il ne parvient pas à faire sans bruit**

**Elle peut alors voir une silhouette aller vers la malle et hausser le ton**

### MamaBallou

##### 28/04/2023 00:03

##### <https://discord.com/channels/@me/503896082045206528/1101267492384559244>

**Comme il n'est jamais deux sans trois, Adaram se prit de nouveau le poing de l'homme dans la figure. Mais cette fois ci, bien plus fort, ce qui eu pour effet de le sonner.**

- Tient toi tranquille j'ai dit !

**Puis l'homme ferme le couvercle de la malle dans laquelle Adaram se trouve.**

**Elle essaye de faire le tour du camp pour se positionner non loin de la cariole. Mais la mousse de tout à l'heure a bougée et elle se prend les pieds dedans et s'étale de tout son long avec un bruit sourd significatif.**

### Mathius

##### 28/04/2023 00:06

##### <https://discord.com/channels/@me/503896082045206528/1101268161782882376>

**Hisako se relève alors avec peine tandis qu'elle entend encore les personnes qui commentent le bruit qu'elle avait fait**

- C'est encore lui ? *hasarde un premier*
- J'en doute *dit le second qui vient d'assommer Adaram*
- Allons voir *conclu le troisième*

### MamaBallou

##### 28/04/2023 00:11

##### <https://discord.com/channels/@me/503896082045206528/1101269486146945125>

**Elle se dépêche de se cache à nouveau mais elle se retrouve alors nez à nez avec des pieds. Dans la gueule du loup. Elle relève les yeux pour n'avoir le temps que de retenir sa repiration et d'attendre le coup arriver**

**Quand Adaram revint à lui, il était lourd. Il avait mal au crâne et surtout il était ballotté.**

**Hisako avait été entassée dans la malle avec lui. D'où le poids en plus. Elle était toujours inconsciente, la cariole était en mouvement et il distinguait la lumière du jour entre les planches de la malle.**

### Mathius

##### 28/04/2023 00:16

##### <https://discord.com/channels/@me/503896082045206528/1101270673223073922>

**Il n'osait pas bouger pour éviter de se refaire assommer mais bougeait malgré tout dû au bringueballement de la cariole et la gravité. Il tente alors timidement de *poke* Hisako pour la réveiller**

### MamaBallou

##### 28/04/2023 00:18

##### <https://discord.com/channels/@me/503896082045206528/1101271272698155089>

**Hisako est out pour le moment mais il entend leurs ravisseurs chanter à tue-tête, il sait qu'ils ne risquent donc pas de l'entendre de toutes manières.**

### Mathius

##### 28/04/2023 00:24

##### <https://discord.com/channels/@me/503896082045206528/1101272814423650376>

**En ayant entendu cela il tente alors de se replacer comme il peut en cherchant une position confortable sans trop appuyer sur Hisako, même si cette dernière semble inapte à réagir de quelque façon que ce soit**

**Pendant ce temps leur kidnappeur continue de siflotter alors qu'un autre semble causer au loin**

### MamaBallou

##### 28/04/2023 00:30

##### <https://discord.com/channels/@me/503896082045206528/1101274152691847308>

**Il arrive à décaler légèrement Hisako et à être plus confortable, tout ça pour qu'une bosse sur la route la ramène sur lui de façon à ce que son oreille droite lui chatouille le nez**

**Il peut entendre les ravisseurs plus distinctement que la veille**

- J'arrive pas à croire qu'elle soit venue toute seule, bouhahahah ! *se bidonne le premier*
- J'aurais jamais fait un travail aussi vite, huhuhuhhh ! *esclaffe le second*
- Aller du calme les gars, c'est pas le moment de faire des conneries, il reste encore de la route avant que ce soit fini.

### Mathius

##### 28/04/2023 10:13

##### <https://discord.com/channels/@me/503896082045206528/1101420931311738900>

**Il tente alors de se décaler sans succès tout en continuant d'entendre les ravisseurs toujours aussi joyeux alors que la calèche roulait à vive allure**

### MamaBallou

##### 28/04/2023 11:01

##### <https://discord.com/channels/@me/503896082045206528/1101432975821901824>

**À un certain point, Hisako se réveilla. Elle remua ses oreilles, captant les sons alentours. Puis elle ouvrit les yeux et se retrouva nez à nez avec Adaram. Chose à laquelle elle ne s'attendait pas du tout. Elle eu un mouvement de recul qui eu pour effet d'écraser davantage Adaram et de la faire rencontrer le couvercle du coffre où ils se trouvait.**

### Mathius

##### 28/04/2023 19:01

##### <https://discord.com/channels/@me/503896082045206528/1101553677250088970>

**Cela provoqua un grand bruit qui sembla attirer les ravisseurs qui donnèrent alors une tape sur la caisse en beuglant** On se calme !

### MamaBallou

##### 28/04/2023 19:09

##### <https://discord.com/channels/@me/503896082045206528/1101555760502149202>

**Hisako écarquilla les yeux et adressa un regard interrogateur à Adaram. Puis lui demanda à voix basse**

- Tu m'expliques ?

### Mathius

##### 28/04/2023 19:13

##### <https://discord.com/channels/@me/503896082045206528/1101556833828749324>

**Tout bas il répondit**

- Tu t'es fait kidnappée par mes ravisseurs et je n'ai aucune idée de qui ils sont, où ils vont ni leur motivation *dit-il avec une voix paniquée*

### MamaBallou

##### 28/04/2023 19:32

##### <https://discord.com/channels/@me/503896082045206528/1101561547555360900>

- Merde... *se contenta-t-elle de pester*

**Elle essaye de se remémorer les évènements**

- Et tu m'as embarqué là dedans, tu te sentais seul dans cette malle. *ironisa-t-elle en soupirant*

### Mathius

##### 28/04/2023 22:11

##### <https://discord.com/channels/@me/503896082045206528/1101601697266155570>

Oui si seul *dit-il en ironisant à son tour alors qu'il commence alors à chercher un détail quelconque dans la malle*

### MamaBallou

##### 28/04/2023 23:43

##### <https://discord.com/channels/@me/503896082045206528/1101624762414923870>

**Il s'agissait malheureusement d'une vieille malle comme il en existe beaucoup.**

- Je vais tenter un truc. *commence-t-elle*
-

**Elle essaye de concentrer sa magie mais est alors prise de douleurs.**

- Arg. La vache ! C'est quoi ce délire ?!

**Adaram aura vu le cou de Hisako s'éclairer d'une lumière violette qui se dissipait maintenant doucement.**

### Mathius

##### 28/04/2023 23:46

##### <https://discord.com/channels/@me/503896082045206528/1101625504899022980>

*Il demande alors à demi mots* Ça va ? *alors qu'il semble tenter de comprendre cette étrange lumière sur la Kitsune*

### MamaBallou

##### 28/04/2023 23:50

##### <https://discord.com/channels/@me/503896082045206528/1101626402333282325>

**Sans rien dire elle essaya de nouveau avec plus de conviction. Cela n'eu pour effet que de la faire souffrir d'avantage.**

- Hurg. *respire lourdement* Non ça ne va pas. *ses yeux était affolé* Ça ne marche pas. Rien ne marche...

**Adaram peut voir que la kitsune commence à paniquer légèrement**

### Mathius

##### 28/04/2023 23:58

##### <https://discord.com/channels/@me/503896082045206528/1101628545161240647>

**Adaram la voyant tenter de faire quelque chose et paniquée tente alors de reprendre le contrôle des mousses comme la dernière fois**

### MamaBallou

##### 29/04/2023 00:02

##### <https://discord.com/channels/@me/503896082045206528/1101629524837404673>

**La magie répondit à son appel. Puis il senti le flux lui échappé et un mur invisible se heurter à lui. S'en suivit une douleur éclair tout le long de son corps.**

**De son côté, Hisako s'était murée dans le silence pour réfléchir à une explication quand elle vit un lueur violette autour du cou de Adaram l'éblouir et elle comprit.**

### Mathius

##### 29/04/2023 00:04

##### <https://discord.com/channels/@me/503896082045206528/1101630010462314637>

**Adaram maugré de douleur à son tour alors qu'il présume comprendre ce qui touche Hisako aussi. Il tente de marmonner**
T.... oi au... ssi ?

### MamaBallou

##### 29/04/2023 00:11

##### <https://discord.com/channels/@me/503896082045206528/1101631872376766565>

**Elle hocha la tête.**

- C'est salopards nous ont mis un collier en adamantite...

**Ce métal très cher et peu répandu avait la caractéristique, quand traité de la bonne manière, de bloquer les pouvoirs magiques et psychiques de son porteur. De plus, une fois mis, seul la personne qui a enchanté le bijou ou personne désignée par l'incantation peut le retirer.**

### Mathius

##### 29/04/2023 00:14

##### <https://discord.com/channels/@me/503896082045206528/1101632549492633661>

Co... comment ? *dit-il surpris par le fait qu'elle connaisse ce genre de chose qu'il ignorait complètement*

Pourquoi dépenser autant de moyen à notre encontre *dit-il assez surpris et agacé ce qui rend son discours beaucoup plus bruyant*

**Leurs ravisseurs tapèrent encore une fois sur la caisse en leur adressant**
On se calme là dedans !

### MamaBallou

##### 29/04/2023 00:24

##### <https://discord.com/channels/@me/503896082045206528/1101635183519744000>

**Elle se crispa légèrement, surprise par les coups contre le coffre.**

- Ce n'est pas un métal rependu. *commença-t-elle en baissant le ton* Je n'en ai moi même qu'entendu parler que dans les histoire d'un de nos ancien, aujourd'hui décédé. Il disait que ce metal empêchait les créatures magiques de s'exprimer. Leur infligeant douleurs à chaque tentative...

### Mathius

##### 29/04/2023 00:26

##### <https://discord.com/channels/@me/503896082045206528/1101635592586018847>

Nous sommes donc des créatures qu'ils ont capturés ? *hazarde-t-il la mort dans l'âme*

### MamaBallou

##### 29/04/2023 00:32

##### <https://discord.com/channels/@me/503896082045206528/1101637019723116644>

**Elle ne répondit pas. Laissant le silence faire son oeuvre. Le temps passa et le soir arriva. Apparemment, ils allaient encore devoir bivouaquer. Les ravisseurs leur autorisèrent de très gênante pause pipi et un repas chaud. Cependant, le regard farouche de Hisako lui valu également une bonne rouste.**

**L'endroit où ils s'étaient arrêté était désert et aucun autre bruit que ceux de la nuit n'habitait les bois. Les ravisseurs les avaient attachés solidement dos à dos à un arbre et se relayait pour les surveiller.**

### Mathius

##### 29/04/2023 00:35

##### <https://discord.com/channels/@me/503896082045206528/1101637789294006333>

**Adaram semblait relativement refermé sur lui même alors qu'il avait un regard d'enterrement comme s'il repassait toute la soirée du kidnapping en boucle.**

**Les deux se trouvaient alors dans cette situation absurdes où ils continuaient de coopérer à des inconnus alors que leurs peuplades avaient du mal et qu'il n'avait aucune idée d'où leurs ravisseurs voulaient les emmener.**

### MamaBallou

##### 29/04/2023 00:39

##### <https://discord.com/channels/@me/503896082045206528/1101638825719115796>

**Le lendemain le voyage repris. Ils étaient de nouveau entassé dans le coffre. Arrivé en milieu de mâtinée, le coffre s'ouvrit et on leur mit des baillons. Un peu plus tard, ils pouvaient entendre les bruits de l'effervescence d'une ville.**

### Mathius

##### 29/04/2023 00:43

##### <https://discord.com/channels/@me/503896082045206528/1101639758054174791>

**Les deux purent sentir que ces derniers naviguaient visiblement de bâtiments en bâtiments car ces derniers sentaient un coup la température externe et quelques instants plus tard une chaleur partielle et un calme à peine présent. Ce manège dura quelques longues dizaines de minutes avant que nos comparses ne purent retrouver un semblant de calme avec leur ravisseurs qui les plaçait au sol.**

### MamaBallou

##### 29/04/2023 00:47

##### <https://discord.com/channels/@me/503896082045206528/1101640797050384534>

**Juste avant que le coffre ne s'ouvre. Adaram aurait pu croire qu'Hisako allait de jeter sur la première personne qu'elle verrait en sortant de là. Cependant, quand on lui intima de sortir, elle obtempéra docilement. Seul ses yeux trahissait sa colère.**

**Les deux captifs furent conduit en sous-sol et séparés dans des geôles différentes mais adjacentes. Ils n'étaient pas les seuls à se trouver là mais les seuls à avoir si peu de blessures**

### Mathius

##### 29/04/2023 00:53

##### <https://discord.com/channels/@me/503896082045206528/1101642458401603604>

**Hisako supposa, surtout après avoir vu l'attitude d'Adaram jusqu'à présent, que ce dernier allait paniquer sans se retenir de quelque façon que ce soit mais elle fut surpris de le voir très calme alors qu'il semblait juste attendre que leurs ravisseurs leur donne un nouvel ordre même s'il semblait lancer des regards aux autres prisonniers**

### MamaBallou

##### 29/04/2023 01:00

##### <https://discord.com/channels/@me/503896082045206528/1101644227898785832>

**Les autres prisonniers n'étaient pour leur part pas des créatures magiques. Aucun d'eux ne portait un collier comme Adaram et Hisako.**

**On entendit alors le bruit de pas réguliers de 3 hommes. Les 3 s'arrêtèrent devant la cellule d'Adaram. Le premier était habillé soigneusement mais sans présomption. Les deux autres semblaient assurer sa sécurité et portaient à leur ceinture de longues épées au fil aiguisé. Tous trois portaient des masques noir. Seul leur yeux brillait dans la semi pénombre du sous sol**

- D'où vient-il celui là ? *questionna l'homme bien habillé*
-

**Un des deux soldat lui chuchota quelque chose à l'oreille en lui tendant un rapport**

- Oh ! Très bien alors ! Deux pour le prix d'un. *conclu l'homme avant de passer à la cellule d'Hisako*

### Mathius

##### 29/04/2023 01:04

##### <https://discord.com/channels/@me/503896082045206528/1101645069695590400>

**Les deux hommes avec le premier se permettent alors de s'approcher de la cellule d'Adaram qui ne pu s'empêcher de faire un pas en arrière relativement craintif.**

**Hisako quand a elle était observé par cet homme qui semblait l'observer dans tout les recoins avant d'aller chuchoter une question de plus au 'marchand'**

### MamaBallou

##### 29/04/2023 01:09

##### <https://discord.com/channels/@me/503896082045206528/1101646295111827536>

- Merveilleux ! *s'exclama alors celui ça* Ils seront les pièces phare de ce soir.

**Hisako et Adaram eurent alors la conviction de l'endroit dans lequel ils se trouvaient. Une enchère clandestine de trafic d'êtres**

**Hisako recula à son tour pour se caler dans un coin de la cellule en attendant que les hommes s'en aille**

### Mathius

##### 29/04/2023 01:14

##### <https://discord.com/channels/@me/503896082045206528/1101647600203092038>

**Adaram la voyant faire du coin de l'oeil se permit de l'imiter alors que 'l'acheteur' pour ce soir semblait ravi de pouvoir les prendre pour sa petite fête.**

Excellent ! *ajouta l'un des deux hommes*

**Alors qu'aussi bien Adaram qu'Hisako pouvait voir le chef des trois hommes semblaient échanger une monnaie avec le revendeur avant qu'il ne lui confie ce qui semblait être deux clés.**

### MamaBallou

##### 29/04/2023 01:22

##### <https://discord.com/channels/@me/503896082045206528/1101649660365504623>

**Hisako suivit avec intérêt les deux clés avant de découvrir qu'elles n'étaient que celles des cellules. Les deux gardes attrapèrent l'un et l'autre les 'marchandises' du soir pour les emmener plus haut. Hisako et Adaram furent de nouveau séparés et placés dans des pièces où plusieurs personnes les attendaient. Ces personnes les obligèrent chacun de leur côté à se rendre présentable. Incluant bain, habillage et tout le tintouin.**

**La pièce n'avait pas de fenêtre et le garde attendait devant la porte. Impossible de s'échapper. Hisako qui s'était tû jusque là, essaya alors d'interroger les femmes qui s'occupaient d'elle**

- Pourriez-vous m'expliquer de qu'il se passe ? *tenta-t-elle* Qui est l'homme qui m'a acheté ? Pourquoi de tels préparatifs ?

### Mathius

##### 29/04/2023 01:29

##### <https://discord.com/channels/@me/503896082045206528/1101651402360623215>

**Hisako remarqua alors que les deux jeunes femmes qui s'occupaient d'elle semblait également porter une forme de collier qui ne laissa pas Hisako de marbre. Une forme de chaine pensa-t-elle alors que les deux jeunes femmes ne semblaient pas comprendre ce qu'Hisako disait et se regardaient interdites alors qu'elles marmonnaient des sons qu'Hisako ne semblait pas comprendre**

### Mathius

##### 29/04/2023 01:32

##### <https://discord.com/channels/@me/503896082045206528/1101652237601751090>

**Adaram quand à lui avait été au même titre qu'Hisako emené afin d'être rendu présentable, ce dernier était relativement timide et n'osait pas prendre la parole en se laissant habiller comme la poupée qu'il supposait devoir être ce soir.**

### MamaBallou

##### 29/04/2023 01:41

##### <https://discord.com/channels/@me/503896082045206528/1101654332585300130>

**Elle se retrouva désemparée mais ne pu rien tirer d'autre que des regards d'incompréhension de la part des jeunes femmes**

**Après plusieurs heures d'habillage et de pouponnage, on fit sortir Hisako et Adaram de leur salle respective. Ils étaient habillés légèrement, trop légèrement à leur goût. Hisako tentait de se couvrir comme elle pouvait avec sa queue et ses bras mais le principale de ses formes étaient visibles.**

**La soirée devait être bien entamée car ils commençaient tous les deux à fatiguer. On les mena derrière un grand rideau et les attachèrent là sans plus d'explication. De l'autre côté, ils percevaient le maitre d'enchère prendre les enchères des participants qui dépensaient parfois des sommes folles**

### Mathius

##### 29/04/2023 01:56

##### <https://discord.com/channels/@me/503896082045206528/1101658110084526120>

**Les deux comparses pouvaient s'échanger des regards gênés et d'incompréhension face à leurs tenues respectives. Alors que les deux murmuraient**

Mais pourquoi dans cette tenue ?

**Alors que l'un comme l'autre dans cette tenue semblaient à moitié reluquer l'autre en disant cela**

### MamaBallou

##### 29/04/2023 02:00

##### <https://discord.com/channels/@me/503896082045206528/1101659281834639360>

**Hisako n'aurait pas pu être plus rouge. Et tourna la tête pour éviter de voir Adaram**

- Je ne sais pas pourquoi mais j'aurais aimer choisir la mienne avant qu'ils me choisissent une couleur si moche. *esseya-t-elle de dédramatiser* Quoi qu'il arrive, il faut qu'on reste ensemble d'accord ? Je ne sais pas encore comment on va faire, mais il faut qu'on essaye. Si on est séparé ce sera plus dur de s'enfuir. *dit-elle sérieusement*

### Mathius

##### 29/04/2023 02:12

##### <https://discord.com/channels/@me/503896082045206528/1101662299921600563>

**Adaram rouge pivoine également semblait tenter de fixer son regard ailleurs même si il s'approche légèrement pour mieux entendre les dires d'Hisako**

Et surtout des vêtements moins court *dit-il en entendant la remarque sur les couleurs de la part d'Hisako, remarque qui ne semble pas aider nos deux comparses*

**Suite à la remarque plus sérieuse de celle-ci, il répond alors sérieusement**

Oui je suis d'accord *alors qu'il se permet de lui saisir la main pour montrer son accord à celle-ci*

### MamaBallou

##### 29/04/2023 02:20

##### <https://discord.com/channels/@me/503896082045206528/1101664269549977691>

**Au contact de la main d'Adaram, Hisako sursauta mais accepta sa main et la serra en retour, rassurée par le contact**

- Si tu es pris avant moi, dit à ton acheteur que je suis douée en magie de feu que je peux lui servir d'escorte discrète. Si ça ne suffit pas, ajoute que je.. suis versée dans l'art de la manipulation mentale... *ajouta-t-elle à demi voix*

**Elle marqua un instant de pause, hésitant à en dire plus. Elle repris une inspiration comme pour ajouter quelque chose, mais ne dit rien de plus**

### Mathius

##### 29/04/2023 02:28

##### <https://discord.com/channels/@me/503896082045206528/1101666311819493546>

**Il resta un temps à attendre la suite de la phrase de celle-ci avant de répondre à son tour**

Dans mon cas tu peux lui évoquer ma magie de contrôle des végétaux et que j'ai une certaine forme de détection à courte portée, mais mais si tu veut *dit-il avant de rougir* j'ai peut-être une solution pour... pour... qu'on nous achète ensemble si tu me fait confiance *dit-il tout rouge alors qu'il serre la main de celle-ci un peu plus fort avant de lui murmurer à l'oreille* Sauf si tu veut finir les arguments que je dois donner à mon éventuel acheteur

### MamaBallou

##### 29/04/2023 02:30

##### <https://discord.com/channels/@me/503896082045206528/1101666885461880844>

**Elle fronça légèrement les sourcils, intriguée mais méfiante.**

- Quel genre de solution ?

### Mathius

##### 29/04/2023 02:32

##### <https://discord.com/channels/@me/503896082045206528/1101667270163439728>

*Il reste honteux en le disant*

Utiliser nos tenues pour faire passer un message *dit-il alors que le rideau commence à se lever tout doucement*

### MamaBallou

##### 29/04/2023 02:35

##### <https://discord.com/channels/@me/503896082045206528/1101668144780689558>

**Hisako comprenait de moins en moins**

- Soit plus précis si tu veux que je comprenne *s'impatienta-t-elle*

**Le présentateur commençait déjà à préparer l'entrée du clou du spectacle de la soirée.**

### Mathius

##### 29/04/2023 02:39

##### <https://discord.com/channels/@me/503896082045206528/1101669032127643728>

**Adaram entendant le rideau se lever se permet alors de saisir l'autre main d'Hisako et de lui dire**

Autant se faire passer pour un couple si on souhaite ne pas être séparé *même s'il reste rouge et honteux il semble dire ça sérieusement alors que le temps presse*

### MamaBallou

##### 29/04/2023 02:49

##### <https://discord.com/channels/@me/503896082045206528/1101671576895758386>

**Hisako figea un instant avant de dégager ses mains de celles d'Adaram**

- Non ces sadiques seraient capable de faire exprès de nous séparer. *trancha-t-elle toujours rouge pivoine* Mais merci pour l'idée.

**Deux paires de bras puissants obligèrent les 2 comparses à s'avancer sur le devant de la scène, plaçant Hisako légèrement en arrière.**

**La foule avait retenu son souffle devant l'aspect des "produits" proposés.**

- Et maintenant, les produits fards de ce soir ! Venu tout droit des populations nomades. Tout d'abord un dryade. Rare son ceux qui en voit encore en ville de nos jour, il fera le bonheur de celui ou celle qui l'acquerra. Tout juste importer mais docile, il permettra à son acquisiteur de tester son habileté à l'éducation de recrues exotique. Son aptitude magique a été jugée forte et son corps est robuste, il sera parfait pour n'importe lequel des travaux que vous pourriez lui assigner. L'enchère commence à 30 pièces d'argents.

### Mathius

##### 29/04/2023 03:05

##### <https://discord.com/channels/@me/503896082045206528/1101675709484769330>

Certes *murmure-t-il d'un air désolé en se rangeant à l'avis de celle-ci avant de se faire embarquer par les bras puissant*

**Durant la présentation Hisako ne peut s'empêcher de reluquer Adaram avant d'être à son tour mise en avant par le garde qui la met alors juste au niveau d'Adaram qui semble si gêné par la situation et par le fait que cette dernière avait l'air de le mater**

- Et voici un des autres produits qui suscitera la convoitise parmi vous ! Elle vient également tout droit des populations nomades, voici une Kitsune. Aussi rare que le produit précédent, son possesseur sera ravi de l'acquérir. Nous venons de l'acquérir et elle a un peu de caractère, l'occasion de l'entrainer à être plus docile. Elle possède aussi une aptitude magique visiblement aussi puissante que notre autre produit et elle sera parfaite pour vous aider au quotidien.
L'enchère débute à 30 pièces d'argent également.

**Durant tout le discours, Adaram n'avait pas pû quitter Hisako des yeux, rouge pivoine mais la dévorant visiblement du regard et constatant tout la minutie qui avait été mise dans ses habbits pour en montrer le plus possible**

**Les gardes avancèrent alors les deux produits au même niveau, ce qui permit aux deux comparses de l'un comme l'autre s'échanger des regards coupables sans pour autant parvenir à regarder autre chose**
